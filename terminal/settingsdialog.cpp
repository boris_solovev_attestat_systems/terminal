#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(TerminalConfig *terminalConfig, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    this->terminalConfig = terminalConfig;

    ui->serialEdt->setText((const QString &)terminalConfig->serial);
    ui->addressEdt->setText(terminalConfig->serverUrl);
    ui->uploadAddressEdt->setText(terminalConfig->uploadServerUrl);
    ui->syncEdt->setValidator( new QIntValidator(30, 1000, this) );
    ui->syncEdt->setText(QString::number(terminalConfig->syncTime));
    ui->softwareVersionLbl->setText(terminalConfig->software);

    connect(this, SIGNAL(accepted()), this, SLOT(saveConfig()));
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::saveConfig() {
    if(this->result() == QDialog::Accepted) {
        terminalConfig->serial = ui->serialEdt->text();
        terminalConfig->serverUrl = ui->addressEdt->text();
        terminalConfig->uploadServerUrl = ui->uploadAddressEdt->text();
        terminalConfig->syncTime = ui->syncEdt->text().toInt();
        terminalConfig->save();
    }
}

