#ifndef CORE_H
#define CORE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <thread>
#include <chrono>
#include <QThread>
#include "timeprovider.h"
#include "eventbuffer.h"
#include "terminalconfig.h"
#include "serverconnection.h"
#include "filedownloader.h"
#include <QHttpMultiPart>
#include <QHttpPart>

#include "realsensecamera.h"

#define STATE_PENDING 0
#define STATE_DONE 1

class Core: public QObject, CameraListener, ServerConnectionListener
{
    Q_OBJECT

    void printfConfig();
public:
    QNetworkReply *currentUploadReply = nullptr;
    QString currentUploadPath;
    QString currentUploadType;
    QString currentUploadChannel;
    QFile *currentUploadfile;

    TerminalConfig config;

    TimeProvider timeCorrection;

    enum OperationState {
        OperationStateUnknown,
        OperationStatePending,
        OperationStateDone,
        OperationStateFailed
    };
    OperationState connectionState = OperationState::OperationStateUnknown;
    OperationState registrationState = OperationState::OperationStateUnknown;
    OperationState uploadState = OperationState::OperationStateUnknown;

    EventBuffer eventReport;
    int eventReported = 0;

    ServerConnection connection;

    bool inWork = false;
    std::thread *worker = nullptr;
    QThread *dispatcherThread;

    long long lastConnectTime = 0;
    long long lastRegistrationTime = 0;

    bool startDiskLimitAlarmFired = false;
    long long startDiskLimitAlarmTime = 0;
    bool stopDiskLimitAlarmFired = false;
    long long stopDiskLimitAlarmTime = 0;
    bool stopRecord = false;

    long long eventReportTime = 0;
    bool updatePending = false;

    vector<shared_ptr<RealSenseCamera>> cams;

    static void run(Core *self);
    void _run();

    explicit Core(QObject *parent = 0);
    ~Core();

    void initializeConnection(ServerConnection &connection);

    void initializeCameras();

    void start();
    void stop();

    void loadConfig();
    void saveConfig();

    void connectServer();
    void disconnectServer();
    QJsonObject createMessageEvent(QString message);
    void addEventReport(QJsonObject jEvent);
    void registration();
    void doEventReport();
    QJsonObject prepareTerminalInfo();
    void sendTerminalInfo();
    void doFileUpload();

    void startInstallSoftware(QWebSocket *connection, int id, QJsonObject args);
    void resetCounters(QWebSocket *connection, int id, QJsonObject args);
    void subscribeStream(QWebSocket *connection, int id, QJsonObject args);
    void unsubscribeStream(QWebSocket *connection, int id, QJsonObject args);
    void readInfo(QWebSocket *connection, int id, QJsonObject args);
    void setTerminalValue(QWebSocket *connection, int id, QJsonObject args);
    void setChannelValue(QWebSocket *connection, int id, QJsonObject args);

    void downloadFile(QString url, QString path);

    void onVideoFrame(cv::Mat &mat, RealSenseCamera *source) override;
    void onDepthFrame(cv::Mat &mat, RealSenseCamera *source) override;
    void onFileReady(string path, string type, int configIndex) override;
    void onNewFile(string path, string type, int configIndex) override;

    void onConnected() override;
    void onDisconnected() override;
    void onServiceConnected() override;
    void onServiceDisconnected() override;

    QString getSystemTemperature();

public slots:
    void onDownloadError(DownloadRequest *request, QString message);
    void onDownloadComplete(DownloadRequest *request);
signals:
    void showFrame(char *data, int width, int height, int stride, int displayIdx);
    void uploadFile(QString terminalSerial, QString fileType, QString channelIdx, QString path, QString fileName, QString sUrl);
};

#endif // CORE_H
