#ifndef EVENTBUFFER_H
#define EVENTBUFFER_H

#include <QReadWriteLock>
#include <QFile>
#include <QTextStream>
#include <QList>
#include <QDebug>

class EventBuffer
{
    QReadWriteLock cs;
    QString fileName;
public:

    EventBuffer(QString fileName) : fileName(fileName) {
    }

    bool addEvent(QString event) {
        bool result = false;

        cs.lockForWrite();
            do {
                QFile file(fileName);

                if(!file.open(QIODevice::WriteOnly | QIODevice::Append)) {
                    qDebug() << "File open error: " + file.errorString() + " for " + file.fileName();
                    break;
                }
                QTextStream in(&file);
                in << event << "\n";
                file.flush();
                file.close();

                result = true;
            } while(false);
        cs.unlock();

        return result;
    }

    QList<QString> getFirst(int count) {
        QList<QString> result;

        cs.lockForRead();
            do {
                QFile file(fileName);

                if(!file.open(QIODevice::ReadOnly)) break;
                QTextStream in(&file);

                int i;
                for(i = 0; i < count && !in.atEnd(); i++) {
                    QString str;
                    str = in.readLine();

                    if(str == "") break;

                    result.push_back(str);
                }
                file.close();

            } while(false);
        cs.unlock();

        return result;
    }

    bool removeFirst(int count) {
        bool result = false;

        cs.lockForWrite();
            QFile src(fileName);
            QFile dst(fileName + "_123");
            do {
                if(!src.open(QIODevice::ReadOnly)) break;
                if(!dst.open(QIODevice::WriteOnly)) break;
                QTextStream in(&src);
                QTextStream out(&dst);

                int i;
                for(i = 0; i < count && !in.atEnd(); i++) {
                    QString str;
                    in >> str;

                    if(str == "") break;
                }
                for(; !in.atEnd(); i++) {
                    QString str;
                    in >> str;

                    if(str == "") break;

                    out << str << "\n";
                }
            }while(false);

            src.close();
            dst.flush();
            dst.close();

            src.remove();
            dst.copy(fileName);

            dst.remove();
        cs.unlock();

        return result;
    }

    bool clean() {
        bool result = false;

        cs.lockForWrite();
            QFile src(fileName);
            src.remove();
        cs.unlock();

        return result;
    }
};

#endif // EVENTBUFFER_H
