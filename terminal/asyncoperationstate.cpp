#include "asyncoperationstate.h"

AsyncOperationState::AsyncOperationState()
{
}

AsyncOperationState::OperaionState AsyncOperationState::getState() {
    std::lock_guard<std::mutex> lock(stateCS);

    return _getState();
}

AsyncOperationState::OperaionState AsyncOperationState::_getState() {
    if(state == OperaionState::PENDING) {
        qint64 now = QDateTime::currentMSecsSinceEpoch();
        if((now - startTime) >= currentTimeout) {
            state = OperaionState::NOT_PERFORMED;
        }
    }

    return state;
}

void AsyncOperationState::_setState(OperaionState state, int timeout) {
    //std::lock_guard<std::mutex> lock(stateCS);

    this->state = state;

    if(state == OperaionState::PENDING) {
        startTime = QDateTime::currentMSecsSinceEpoch();
        currentTimeout = timeout;
    }
}
