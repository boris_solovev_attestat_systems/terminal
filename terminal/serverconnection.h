#ifndef SERVERCONNECTION_H
#define SERVERCONNECTION_H

#include <QObject>
#include <QWebSocket>
#include <QWebSocketServer>
#include <QMap>
#include <QJsonDocument>
#include <QJsonObject>
#include <mutex>

Q_DECLARE_METATYPE(std::function<void(QJsonObject args)>)

class ServerConnectionListener {
public:
    virtual void onConnected() = 0;
    virtual void onDisconnected() = 0;
    virtual void onServiceConnected() = 0;
    virtual void onServiceDisconnected() = 0;
};

class ServerConnection : public QObject
{
    Q_OBJECT

    QWebSocketServer *m_pWebSocketServer;

    int callId = 0;
    std::mutex callCS;

    bool pendingDisconnect = false;

    class Callback {
    public:
        std::function<void(QWebSocket *connection, QJsonObject args)> onResult;
        std::function<void(QWebSocket *connection, QJsonObject args)> onFail;

        void set(std::function<void(QWebSocket *connection, QJsonObject args)> onResult, std::function<void(QWebSocket *connection, QJsonObject args)> onFail) {
            this->onResult = onResult;
            this->onFail = onFail;
        }
    };

    QMap<int, Callback> callbacks;
    quint64 pendingStateStart = 0;

    ServerConnectionListener *listener;

public:
    QWebSocket m_webSocket;
    QWebSocket *m_serviceSocket = nullptr;

    QMap<QString, QMap<QString, std::function<void(QWebSocket *connection, int id, QJsonObject args)>>> iface;

    QString m_url;

    enum ConnectionState {
        DISCONNECTED,
        PENDING,
        CONNECTED
    };
    ConnectionState state = ConnectionState::DISCONNECTED;

    explicit ServerConnection(QObject *parent = nullptr, ServerConnectionListener *listener = nullptr);
    ~ServerConnection();

    ConnectionState getState();
signals:
    void connected();
    void closed();
    void error();

private slots:
    bool connect(QString url);
    bool disconnect();
    bool call(QWebSocket *connection, QString interface, QString method, QJsonObject args, std::function<void(QWebSocket *connection, QJsonObject args)> onResult, std::function<void(QWebSocket *connection, QJsonObject args)> onFail);
    bool sendBinary(QWebSocket *connection, QByteArray data);
    bool sendText(QWebSocket *connection, QString data);

    bool answer(QWebSocket *connection, int id, QJsonObject args);
    bool error(QWebSocket *connection, int id, QString message);

    void onTextMessageReceived(QWebSocket *connection, QString message);

    void onConnected();
    void onServerMessageReceived(QString message);
    void onClosed();

    void onServiceConnected();
    void onServiceMessageReceived(QString message);
    void onServiceClosed();
};

#endif // SERVERCONNECTION_H
