#include "core.h"
#include <QNetworkInterface>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QThread>
#include <QStringList>
#include <QStorageInfo>
#include <QApplication>
#include <QStorageInfo>

Core::Core(QObject *parent) : QObject(parent), eventReport(EVENT_STORAGE), connection(this, this)
{
    printf("load config...\n");
    loadConfig();
    printf("load config...done\n");
    printf("save config...\n");
    saveConfig();
    printf("save config...done\n");

    printf("init connections...\n");
    initializeConnection(connection);
    printf("init connections...done\n");
}

Core::~Core() {
    stop();
}

QString Core::getSystemTemperature() {
    QString result;

    try {
        std::ifstream file("/sys/class/thermal/thermal_zone0/temp");

        std::string str;
        while (std::getline(file, str))
        {
            result = QString::fromStdString(str);
        }
    } catch(...) {

    }

    return result;
}

void Core::initializeConnection(ServerConnection &connection) {
    connection.iface["control"]["installSoftware"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        startInstallSoftware(connection, id, args);
    };

    connection.iface["control"]["resetCounters"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        resetCounters(connection, id, args);
    };

    connection.iface["control"]["subscribeStream"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        subscribeStream(connection, id, args);
    };

    connection.iface["control"]["unsubscribeStream"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        unsubscribeStream(connection, id, args);
    };

    connection.iface["control"]["readInfo"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        readInfo(connection, id, args);
    };

    connection.iface["control"]["setTerminalValue"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        setTerminalValue(connection, id, args);
    };

    connection.iface["control"]["setChannelValue"] = [&](QWebSocket *connection, int id, QJsonObject args) {
        setChannelValue(connection, id, args);
    };
}

void Core::start() {
    dispatcherThread = QThread::currentThread();
    inWork = true;
    worker = new std::thread(Core::run, this);
}

void Core::stop() {
    inWork = false;
    if(worker != nullptr) worker->join();
    worker = nullptr;

    disconnectServer();
}

void Core::run(Core *self) {
    self->_run();
}

void Core::initializeCameras() {
    rs2::context context;
    rs2::device_list devices = context.query_devices();

    try {
        bool freeIndexes[4];
        bool freeConfigs[4];
        for(int ci = 0; ci < 4; ci++) {
            freeIndexes[ci] = true;
            freeConfigs[ci] = true;
        }

        for (int i = 0; i < devices.size(); i++) {
            string info = devices[i].get_info(rs2_camera_info::RS2_CAMERA_INFO_NAME);
            if (info.find("RealSense") == string::npos) continue;

            shared_ptr<RealSenseCamera> camera = make_shared<RealSenseCamera>();
            camera->terminalId = config.serial.toStdString();
            camera->listener = this;

            string _serial = devices[i].get_info(rs2_camera_info::RS2_CAMERA_INFO_SERIAL_NUMBER);
            for(int ci = 0; ci < 4; ci++) {
                QString serial = QString::fromStdString(_serial);
                if(config.channels[ci].serial == serial || config.channels[ci].serial == "") {
                    config.channels[ci].serial = serial;
                    config.channels[ci].model = QString::fromStdString(info);
                    camera->configIndex = ci;
                    freeConfigs[ci] = false;
                    if(config.channels[ci].index != -1) freeIndexes[config.channels[ci].index] = false;

                    break;
                }
            }
            camera->init(devices[i]);
            cams.push_back(camera);
        }

        for(int ci = 0; ci < cams.size(); ci++) {
            if(cams[ci]->configIndex == -1){
                int di;
                for(di = 0; di < 4; di++)
                    if(freeConfigs[di]) {
                        cams[ci]->configIndex = di;
                        config.channels[di].serial = QString::fromStdString(cams[ci]->serial);
                        config.channels[di].model = QString::fromStdString(cams[ci]->model);
                        freeConfigs[di] = false;
                        break;
                    }
            }
        }

        for(int ci = 0; ci < cams.size(); ci++) {
            if(cams[ci]->configIndex == -1) continue;
            if(config.channels[cams[ci]->configIndex].index == -1) {
                int di;
                for(di = 0; di < 4; di++)
                    if(freeIndexes[di]) {
                        config.channels[cams[ci]->configIndex].index = di;
                        freeIndexes[di] = false;
                        break;
                    }
            }
        }

        for(int ci = 0; ci < 4; ci++)
            if(freeConfigs[ci]) config.channels[ci].index = -1;

        config.save();

        for(int ci = 0; ci < cams.size(); ci++) {
            RealSenseCamera *camera = cams[ci].get();
            camera->open();

            /*camera->counter->config.lowThreshold = 15;
            camera->counter->config.highThreshold = 100;
            camera->counter->config.erosion_size = 1;
            camera->counter->config.minSquare = 100;
            camera->counter->config.sNear = 30;
            camera->counter->config.outerBarrier = 40;
            camera->counter->config.innerBarrier = 150;
            camera->counter->config.maxObjectFrames = 6;*/

            camera->counter->config.lowThreshold = config.channels[camera->configIndex].lowThreshold;
            camera->counter->config.highThreshold = config.channels[camera->configIndex].highThreshold;
            camera->counter->config.erosion_size = config.channels[camera->configIndex].erosion_size;
            camera->counter->config.minSquare = config.channels[camera->configIndex].objectSizeLimit;
            camera->counter->config.sNear = config.channels[camera->configIndex].sNear;
            camera->counter->config.outerBarrier = config.channels[camera->configIndex].highLinePos;
            camera->counter->config.innerBarrier = config.channels[camera->configIndex].lowLinePos;
            camera->counter->config.maxObjectFrames = config.channels[camera->configIndex].maxObjectFrames;
        }
    } catch(...) {
        qDebug() << "Not a realsense camera";
    }

    printfConfig();
}

void Core::printfConfig() {
    qDebug() << "Configs:";
    for(int i = 0; i < 4; i++) {
        qDebug() << "Model:" << config.channels[i].model;
        qDebug() << "Serial:" << config.channels[i].serial;
        qDebug() << "Index:" << config.channels[i].index;
        qDebug() << "ShowDepth:" << config.channels[i].showDepth;
    }
}

void Core::_run() {
    printf("initializeCameras...\n");
    initializeCameras();
    printf("initializeCameras...done\n");

    printf("Restart event...\n");
    QJsonObject e;
    e["type"] = "restart";
    addEventReport(e);
    printf("Restart event...done\n");

    while(inWork) {
        if(cams.size() == 0) std::this_thread::sleep_for(std::chrono::milliseconds(500));

        for (int i = 0; i < cams.size(); i++) {
            cams[i]->terminalId = config.serial.toStdString();
            cams[i]->counter->config.lowThreshold = config.channels[cams[i]->configIndex].lowThreshold;
            cams[i]->counter->config.highThreshold = config.channels[cams[i]->configIndex].highThreshold;
            cams[i]->counter->config.erosion_size = config.channels[cams[i]->configIndex].erosion_size;
            cams[i]->counter->config.minSquare = config.channels[cams[i]->configIndex].objectSizeLimit;
            cams[i]->counter->config.sNear = config.channels[cams[i]->configIndex].sNear;
            cams[i]->counter->config.outerBarrier = config.channels[cams[i]->configIndex].highLinePos;
            cams[i]->counter->config.innerBarrier = config.channels[cams[i]->configIndex].lowLinePos;
            cams[i]->counter->config.maxObjectFrames = config.channels[cams[i]->configIndex].maxObjectFrames;

            int inCount = cams[i]->counter->cnt_in;
            int outCount = cams[i]->counter->cnt_out;

            bool startDiskLimitAlarm = config.storageStartLimitReached();
            bool stopDiskLimitAlarm = config.storageStopLimitReached();

            if(startDiskLimitAlarm && (timeCorrection.getLocalTime() - startDiskLimitAlarmTime) > (60 * 60)) {
                startDiskLimitAlarmTime = timeCorrection.getLocalTime();

                startDiskLimitAlarmFired = true;

                QJsonObject e;
                e["type"] = "startDiskLimitAlarm";
                e["data"] = config.storageSize;
                addEventReport(e);
            }
            if(!startDiskLimitAlarm && startDiskLimitAlarmFired) {
                startDiskLimitAlarmFired = false;

                QJsonObject e;
                e["type"] = "startDiskLimitRecovery";
                e["data"] = config.storageSize;
                addEventReport(e);
            }
            if(!startDiskLimitAlarm) stopRecord = false;

            if(stopDiskLimitAlarm && (timeCorrection.getLocalTime() - stopDiskLimitAlarmTime) > (60 * 60)) {
                stopDiskLimitAlarmTime = timeCorrection.getLocalTime();

                stopDiskLimitAlarmFired = true;

                QJsonObject e;
                e["type"] = "stopDiskLimitAlarm";
                e["data"] = config.storageSize;
                addEventReport(e);
            }
            if(!stopDiskLimitAlarm && stopDiskLimitAlarmFired) {
                stopDiskLimitAlarmFired = false;

                QJsonObject e;
                e["type"] = "stopDiskLimitRecovery";
                e["data"] = config.storageSize;
                addEventReport(e);
            }
            if(stopDiskLimitAlarm) stopRecord = true;

            cams[i]->handleFrames(
                        config.channels[cams[i]->configIndex].recordVideo && !stopRecord,
                        config.channels[cams[i]->configIndex].recordDepth && !stopRecord,
                        config.channels[cams[i]->configIndex].doAnalyze && !stopRecord,
                        config.channels[cams[i]->configIndex].reverse == 1);

            int idx = cams[i]->configIndex;
            if(inCount != cams[i]->counter->cnt_in && idx != -1) {
                QJsonObject e;
                e["channel"] = config.channels[cams[i]->configIndex].serial;
                e["type"] = "in_count";
                e["data"] = (long long)cams[i]->counter->cnt_in - inCount;
                addEventReport(e);

                if(connection.m_serviceSocket != nullptr) {
                    e["index"] = config.channels[cams[i]->configIndex].index;
                    QJsonDocument doc(e);

                    QMetaObject::invokeMethod(&connection, "call",
                      Qt::QueuedConnection,
                      Q_ARG(QWebSocket*, connection.m_serviceSocket),
                      Q_ARG(QString, "control"),
                      Q_ARG(QString, "event"),
                      Q_ARG(QJsonObject, e),
                      Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
                        [&](QWebSocket *connection, QJsonObject args) {
                        }),
                     Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
                        [&](QWebSocket *connection, QJsonObject args) {
                        })
                    );
                }
            }

            if(outCount != cams[i]->counter->cnt_out && idx != -1) {
                QJsonObject e;
                e["channel"] = config.channels[cams[i]->configIndex].serial;
                e["type"] = "out_count";
                e["data"] = (long long)cams[i]->counter->cnt_out - outCount;
                addEventReport(e);

                if(connection.m_serviceSocket != nullptr) {
                    e["index"] = config.channels[cams[i]->configIndex].index;
                    QJsonDocument doc(e);

                    QMetaObject::invokeMethod(&connection, "call",
                      Qt::QueuedConnection,
                      Q_ARG(QWebSocket*, connection.m_serviceSocket),
                      Q_ARG(QString, "control"),
                      Q_ARG(QString, "event"),
                      Q_ARG(QJsonObject, e),
                      Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
                        [&](QWebSocket *connection, QJsonObject args) {
                        }),
                     Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
                        [&](QWebSocket *connection, QJsonObject args) {
                        })
                    );
                }
            }
        }

        doFileUpload();

        long long currentSec = timeCorrection.getLocalTime();

        if(config.changed) {
            config.changed = false;

            if(connection.m_url != config.serverUrl) {
                registrationState = OperationState::OperationStateUnknown;
                disconnectServer();
            } else {
                if(connectionState == OperationState::OperationStateDone) {
                    sendTerminalInfo();
                }
            }
        }

        if(connectionState == OperationState::OperationStateUnknown
           || (connectionState == OperationState::OperationStatePending && (currentSec - lastConnectTime) > 3))
        {
            registrationState = OperationStateUnknown;
            if(!config.serverUrl.isEmpty()) connectServer();

            lastConnectTime = timeCorrection.getLocalTime();
            continue;
        }

        if(connectionState != OperationStateDone) continue;

        if(registrationState == OperationStateUnknown
           || (registrationState == OperationStatePending && (currentSec - lastRegistrationTime) > 3)) {
            if(!config.serial.isEmpty()) registration();

            lastRegistrationTime = timeCorrection.getLocalTime();
            continue;
        }

        if ((eventReportTime + config.syncTime) < currentSec) {
            doEventReport();

            eventReportTime = currentSec;
        }

        //std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

void Core::onVideoFrame(cv::Mat &mat, RealSenseCamera *source) {
    if(config.channels[source->configIndex].videoStreamClients > 0 || config.channels[source->configIndex].s_videoStreamClients > 0) {
        vector<uchar> buffer;
        vector<int> param = vector<int>(2);
        param[0]=CV_IMWRITE_JPEG_QUALITY;
        param[1]=60;

        cv::imencode(".jpg", mat, buffer, param);

        QByteArray data;
        QByteArray qData((const char*)buffer.data(), buffer.size());
        data.append((char)config.channels[source->configIndex].index);
        data.append((char)0);
        data.append(qData);

        if(config.channels[source->configIndex].videoStreamClients > 0) QMetaObject::invokeMethod(&connection, "sendBinary", Qt::QueuedConnection, Q_ARG(QWebSocket*, &connection.m_webSocket), Q_ARG(QByteArray, data));
        if(config.channels[source->configIndex].s_videoStreamClients > 0) QMetaObject::invokeMethod(&connection, "sendBinary", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection.m_serviceSocket), Q_ARG(QByteArray, data));
    }

    if(config.channels[source->configIndex].infoStreamClients > 0 || config.channels[source->configIndex].s_infoStreamClients > 0) {
        vector<uchar> buffer;
        vector<int> param = vector<int>(2);
        param[0]=CV_IMWRITE_JPEG_QUALITY;
        param[1]=60;

        cv::imencode(".jpg", source->gsSrc, buffer, param);

        QByteArray data;
        QByteArray qData((const char*)buffer.data(), buffer.size());
        data.append((char)config.channels[source->configIndex].index);
        data.append((char)2);
        data.append(qData);

        if(config.channels[source->configIndex].infoStreamClients > 0) QMetaObject::invokeMethod(&connection, "sendBinary", Qt::QueuedConnection, Q_ARG(QWebSocket*, &connection.m_webSocket), Q_ARG(QByteArray, data));
        if(config.channels[source->configIndex].s_infoStreamClients > 0) QMetaObject::invokeMethod(&connection, "sendBinary", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection.m_serviceSocket), Q_ARG(QByteArray, data));
    }

    if(config.channels[source->configIndex].showDepth) return;

    if(config.channels[source->configIndex].showInfo) {
        cv::Mat &mat = source->gsSrc;

        int imageSize = mat.rows * mat.step1();
        char *buffer = new char[imageSize];
        memcpy(buffer, mat.data, imageSize);

        emit showFrame(buffer, mat.cols, mat.rows, mat.step1(), config.channels[source->configIndex].index);
    } else {
        int imageSize = mat.rows * mat.step1();
        char *buffer = new char[imageSize];
        memcpy(buffer, mat.data, imageSize);

        emit showFrame(buffer, mat.cols, mat.rows, mat.step1(), config.channels[source->configIndex].index);
    }
}

void Core::onDepthFrame(cv::Mat &mat, RealSenseCamera *source)  {
    if(config.channels[source->configIndex].depthStreamClients > 0 || config.channels[source->configIndex].s_depthStreamClients > 0) {
        vector<uchar> buffer;
        vector<int> param = vector<int>(2);
        param[0]=CV_IMWRITE_JPEG_QUALITY;
        param[1]=60;

        cv::imencode(".jpg", mat, buffer, param);

        QByteArray data;
        QByteArray qData((const char*)buffer.data(), buffer.size());
        data.append((char)config.channels[source->configIndex].index);
        data.append((char)1);
        data.append(qData);

        if(config.channels[source->configIndex].depthStreamClients > 0) QMetaObject::invokeMethod(&connection, "sendBinary", Qt::QueuedConnection, Q_ARG(QWebSocket*, &connection.m_webSocket), Q_ARG(QByteArray, data));
        if(config.channels[source->configIndex].s_depthStreamClients > 0) QMetaObject::invokeMethod(&connection, "sendBinary", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection.m_serviceSocket), Q_ARG(QByteArray, data));
    }

    if(!config.channels[source->configIndex].showDepth) return;
    if(config.channels[source->configIndex].showInfo) return;

    int imageSize = mat.rows * mat.step1();
    char *buffer = new char[imageSize];
    memcpy(buffer, mat.data, imageSize);

    emit showFrame(buffer, mat.cols, mat.rows, mat.step1(), config.channels[source->configIndex].index);
}

void Core::onFileReady(string path, string type, int configIndex) {
    QFileInfo fi(QString::fromStdString(path));

    //if(fi.size() == 0) {
    //    QFile::remove(QString::fromStdString(path));
    //} else {
        config.addStorageSize(fi.size());
        /*fileList.addEvent(
                    QString::fromStdString(path) + ',' +
                    QString::number(fi.size()) + ',' +
                    QString::fromStdString(type) + ',' +
                    QString::number(config.channels[configIndex].index));*/

        QJsonObject e;
        e["type"] = "recordDone";
        e["data"] = QString::fromStdString(path);

        QString _path = QString::fromStdString(path);
        e["path"] = _path.mid(0, _path.length() - 6);
        e["file_type"] = QString::fromStdString(type);
        e["channel"] = config.channels[configIndex].serial;
        e["size"] = QString::number(QFileInfo(QString::fromStdString(path)).size());
        addEventReport(e);

        QJsonObject e2;
        e2["type"] = "storage_size";
        e2["data"] = config.storageSize;
        addEventReport(e2);
    //}
}

void Core::onNewFile(string path, string type, int configIndex) {
    QJsonObject e;
    e["type"] = "recordStart";
    e["data"] = QString::fromStdString(path);
    e["path"] = QString::fromStdString(path);
    e["file_type"] = QString::fromStdString(type);
    e["channel"] = config.channels[configIndex].serial;
    addEventReport(e);
}

void Core::onConnected(){
    qDebug() << "connected";
    connectionState = OperationStateDone;
}

void Core::onDisconnected() {
    qDebug() << "disconnected";
    connectionState = OperationStateUnknown;

    for(int i = 0; i < 4; i++) {
        config.channels[i].videoStreamClients = 0;
        config.channels[i].depthStreamClients = 0;
        config.channels[i].infoStreamClients = 0;
    }
}

void Core::onServiceConnected() {

}

void Core::onServiceDisconnected() {
    for(int i = 0; i < 4; i++) {
        config.channels[i].s_videoStreamClients = 0;
        config.channels[i].s_depthStreamClients = 0;
        config.channels[i].s_infoStreamClients = 0;
    }
}

void Core::connectServer() {
    connectionState = OperationStatePending;
    qDebug() << "connect...";
    QMetaObject::invokeMethod(&connection, "connect", Qt::QueuedConnection, Q_ARG(QString, config.serverUrl));
}

void Core::disconnectServer() {
    qDebug() << "disconnect...";
    QMetaObject::invokeMethod(&connection, "disconnect", Qt::QueuedConnection);
    connectionState = OperationStateUnknown;
}

void Core::loadConfig() {
    config.load();

    config.storageSize = 0;

    QDir storage(STORAGE_FOLDER);
    QFileInfoList files = storage.entryInfoList(QDir::Files);

    for(int i = 0; i < files.size(); i++) {
        config.storageSize += files[i].size();

        if(files[i].absoluteFilePath().indexOf(".ready") == -1) {
            rename(files[i].absoluteFilePath().toStdString().c_str(), (files[i].absoluteFilePath() + ".ready").toStdString().c_str());

            QStringList parts = files[i].absoluteFilePath().split('-');
            QString fileType = "" + parts[4][0];
            int configIndex = parts[3].QString::toInt();

            QJsonObject e;
            QString _path = files[i].absoluteFilePath();
            e["path"] = _path;
            e["file_type"] = fileType;
            e["channel"] = config.channels[configIndex].serial;
            e["type"] = "recordDone";
            e["size"] = QString::number(files[i].size());
            addEventReport(e);
        }
    }

    printf("***StorageSize: %lld\n", config.storageSize);
}

void Core::saveConfig() {
    config.save();
}

void Core::registration() {
    qDebug() << "register...";

    if(registrationState == OperationStatePending) return;
    registrationState = OperationStatePending;

    QJsonObject args;
    args["serial"] = config.serial;
    args["secret"] = config.secret;
    QMetaObject::invokeMethod(&connection, "call",
                              Qt::QueuedConnection,
                              Q_ARG(QWebSocket*, &connection.m_webSocket),
                              Q_ARG(QString, "basic"),
                              Q_ARG(QString, "register"),
                              Q_ARG(QJsonObject, args),
                              Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
                                [&](QWebSocket* connection, QJsonObject args) {
                                    QString result = args["result"].toString();
                                    qDebug() << "registration: " << result;

                                    if(result == "ok") {
                                        long long time = (long long)args["time"].toDouble();
                                        int zoneOffset = 0; //root["utcp"].toInt();
                                        timeCorrection.set(time, zoneOffset);

                                        registrationState = OperationStateDone;

                                        sendTerminalInfo();
                                    } else {
                                        registrationState = OperationStateUnknown;
                                    }
                                }),
                             Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
                                [&](QWebSocket *connection, QJsonObject args) {
                                    qDebug() << "registration error: " << args["error"].toString();
                                    registrationState = OperationStateUnknown;
                                })
    );
}

QJsonObject Core::prepareTerminalInfo() {
    QStorageInfo diskInfo = QStorageInfo::root();

    QJsonObject args;
    args["serial"] = config.serial;
    args["syncTime"] = config.syncTime;
    args["storage_total"] = diskInfo.bytesTotal();
    args["storage_used"] = diskInfo.bytesTotal() - diskInfo.bytesFree();
    args["startStorageLimit"] = config.startStorageLimit;
    args["stopStorageLimit"] = config.stopStorageLimit;
    if(updatePending) args["software"] = config.software + "(обновляется)";
    else args["software"] = config.software;
    args["storage_size"] = config.storageSize;
    args["uploadServerUrl"] = config.uploadServerUrl;

    QJsonArray channels;
    for(int i = 0; i < 4; i++) {
        if(config.channels[i].index == -1) continue;

        QJsonObject channel;

        channel["serial"] = config.channels[i].serial;
        channel["model"] = config.channels[i].model;
        channel["lowLinePos"] = config.channels[i].lowLinePos;
        channel["highLinePos"] = config.channels[i].highLinePos;
        channel["objectSizeLimit"] = config.channels[i].objectSizeLimit;

        channel["recordVideo"] = config.channels[i].recordVideo?1:0;
        channel["recordDepth"] = config.channels[i].recordDepth?1:0;
        channel["doAnalyze"] = config.channels[i].doAnalyze?1:0;

        channel["index"] = config.channels[i].index;
        channel["door"] = config.channels[i].door;

        channel["lowThreshold"] = config.channels[i].lowThreshold;
        channel["highThreshold"] = config.channels[i].highThreshold;
        channel["erosion_size"] = config.channels[i].erosion_size;
        channel["sNear"] = config.channels[i].sNear;
        channel["maxObjectFrames"] = config.channels[i].maxObjectFrames;

        channel["reverse"] = config.channels[i].reverse;

        channels.push_back(channel);
    }

    args["channels"] = channels;

    return args;
}

void Core::sendTerminalInfo() {

    QJsonObject args = Core::prepareTerminalInfo();

    QMetaObject::invokeMethod(&connection, "call",
      Qt::QueuedConnection,
      Q_ARG(QWebSocket*, &connection.m_webSocket),
      Q_ARG(QString, "control"),
      Q_ARG(QString, "terminalInfo"),
      Q_ARG(QJsonObject, args),
      Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
        [&](QWebSocket *connection, QJsonObject args) {
        }),
      Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
        [&](QWebSocket *connection, QJsonObject args) {
            qDebug() << "send terminal info error: " << args["error"].toString();
        })
    );
}

QJsonObject Core::createMessageEvent(QString message) {
    QJsonObject evt;
    evt["type"] = "message";
    evt["data"] = message;

    return evt;
}

void Core::addEventReport(QJsonObject jEvent) {
    jEvent["time"] = timeCorrection.getServerTime() * 1000;

    QJsonDocument doc(jEvent);
    eventReport.addEvent(QString(doc.toJson(QJsonDocument::Compact)));
}

void Core::startInstallSoftware(QWebSocket *connection, int id, QJsonObject args) {
    qDebug() << "start is called";

    updatePending = true;
    sendTerminalInfo();

    QJsonObject result;
    result["result"] = "ok";
    QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));

    downloadFile(args["link"].toString(), UPDATE_STORAGE);
}

void Core::resetCounters(QWebSocket *connection, int id, QJsonObject args) {
    qDebug() << "reset is called";

    for(int i = 0; i < cams.size(); i++) {
        cams[i]->counter->cnt_in = 0;
        cams[i]->counter->cnt_out = 0;

        QJsonObject ie;
        ie["channel"] = config.channels[cams[i]->configIndex].serial;
        ie["type"] = "in_count";
        ie["data"] = (long long)cams[i]->counter->cnt_in;
        addEventReport(ie);

        QJsonObject oe;
        oe["channel"] = config.channels[cams[i]->configIndex].serial;
        oe["type"] = "out_count";
        oe["data"] = (long long)cams[i]->counter->cnt_out;
        addEventReport(oe);
    }

    QJsonObject result;
    result["result"] = "ok";
    QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));

    doEventReport();
}

void Core::subscribeStream(QWebSocket *connection, int id, QJsonObject args) {
    qDebug() << "subscribe stream";

    for(int i = 0; i < 4; i++) {
        if(config.channels[i].index == args["index"].toInt()) {
            switch(args["type"].toInt()) {
            case 0: // video
                if(connection == &this->connection.m_webSocket) config.channels[i].videoStreamClients++;
                else config.channels[i].s_videoStreamClients++;
                break;
            case 1: // depth
                if(connection == &this->connection.m_webSocket) config.channels[i].depthStreamClients++;
                else config.channels[i].s_depthStreamClients++;
                break;
            case 2: // info
                if(connection == &this->connection.m_webSocket) config.channels[i].infoStreamClients++;
                else config.channels[i].s_infoStreamClients++;
                break;
            }
            break;
        }
    }

    QJsonObject result;
    result["result"] = "ok";
    QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));
}

void Core::unsubscribeStream(QWebSocket *connection, int id, QJsonObject args) {
    qDebug() << "unsubscribe stream";

    for(int i = 0; i < 4; i++) {
        if(config.channels[i].index == args["index"].toInt()) {
            switch(args["type"].toInt()) {
            case 0: // video
                if(connection == &this->connection.m_webSocket) config.channels[i].videoStreamClients--;
                else config.channels[i].s_videoStreamClients--;
                break;
            case 1: // depth
                if(connection == &this->connection.m_webSocket) config.channels[i].depthStreamClients--;
                else config.channels[i].s_depthStreamClients--;
                break;
            case 2: // info
                if(connection == &this->connection.m_webSocket) config.channels[i].infoStreamClients--;
                else config.channels[i].s_infoStreamClients--;
                break;
            }
            break;
        }
    }

    QJsonObject result;
    result["result"] = "ok";
    QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));
}

void Core::readInfo(QWebSocket *connection, int id, QJsonObject args) {
    QJsonObject info = prepareTerminalInfo();

    QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, info));
}

void Core::setTerminalValue(QWebSocket *connection, int id, QJsonObject args) {
    try {
        QString valueName = args["valueName"].toString();
        QString value = args["value"].toString();

        bool saved = true;
        if(valueName == "syncTime") config.syncTime = value.toInt();
        else if(valueName == "startStorageLimit") config.startStorageLimit = value.toLongLong();
        else if(valueName == "stopStorageLimit") config.stopStorageLimit = value.toLongLong();
        else if(valueName == "uploadServerUrl") config.uploadServerUrl = value;
        else saved = false;

        if(!saved) {
            QJsonObject result;
            result["error"] = "No such value";
            QMetaObject::invokeMethod(&this->connection, "error", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));
        } else {
            QJsonObject result;
            result["result"] = "ok";
            QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));

            config.save();
        }
    } catch(...) {
        QJsonObject result;
        result["error"] = "error";
        QMetaObject::invokeMethod(&this->connection, "error", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));
    }
}

void Core::setChannelValue(QWebSocket *connection, int id, QJsonObject args) {
    try {
        int index = args["index"].toInt();
        QString valueName = args["valueName"].toString();
        QString value = args["value"].toString();

        for(int i = 0; i < 4; i++) {
            if(config.channels[i].index == index) {
                bool saved = true;
                if(valueName == "lowLinePos") config.channels[i].lowLinePos = value.toInt();
                else if(valueName == "highLinePos") config.channels[i].highLinePos = value.toInt();
                else if(valueName == "objectSizeLimit") config.channels[i].objectSizeLimit = value.toInt();
                else if(valueName == "recordVideo") config.channels[i].recordVideo = value.toInt();
                else if(valueName == "recordDepth") config.channels[i].recordDepth = value.toInt();
                else if(valueName == "doAnalyze") config.channels[i].doAnalyze = value.toInt();
                else if(valueName == "index") config.channels[i].index = value.toInt();
                else if(valueName == "door") config.channels[i].door = value;
                else if(valueName == "lowThreshold") config.channels[i].lowThreshold = value.toInt();
                else if(valueName == "highThreshold") config.channels[i].highThreshold = value.toInt();
                else if(valueName == "erosion_size") config.channels[i].erosion_size = value.toInt();
                else if(valueName == "sNear") config.channels[i].sNear = value.toInt();
                else if(valueName == "maxObjectFrames") config.channels[i].maxObjectFrames = value.toInt();
                else if(valueName == "reverse") config.channels[i].reverse = value.toInt();
                else saved = false;

                if(!saved) {
                    QJsonObject result;
                    result["error"] = "No such value";
                    QMetaObject::invokeMethod(&this->connection, "error", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));
                } else {
                    QJsonObject result;
                    result["result"] = "ok";
                    QMetaObject::invokeMethod(&this->connection, "answer", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));

                    config.save();
                }

                return;
            }
        }

        QJsonObject result;
        result["error"] = "No such channel";
        QMetaObject::invokeMethod(&this->connection, "error", Qt::QueuedConnection, Q_ARG(int, id), Q_ARG(QJsonObject, result));

    } catch(...) {
        QJsonObject result;
        result["error"] = "error";
        QMetaObject::invokeMethod(&this->connection, "error", Qt::QueuedConnection, Q_ARG(QWebSocket*, connection), Q_ARG(int, id), Q_ARG(QJsonObject, result));
    }
}

void Core::doEventReport() {
    qDebug() << "doEventReport";

    QString t = getSystemTemperature();
    QJsonObject e;
    e["type"] = "temp";
    e["data"] = t;
    addEventReport(e);

    QList<QString> events = eventReport.getFirst(100);

    qDebug() << "report " << events.length() << "events";

    if(events.size() == 0) {
        eventReportTime = timeCorrection.getLocalTime();

        QJsonObject args;

        QMetaObject::invokeMethod(&connection, "call",
          Qt::QueuedConnection,
          Q_ARG(QWebSocket*, &connection.m_webSocket),
          Q_ARG(QString, "control"),
          Q_ARG(QString, "ping"),
          Q_ARG(QJsonObject, args),
          Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
            [&](QWebSocket *connection, QJsonObject args) {
            }),
         Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
            [&](QWebSocket *connection, QJsonObject args) {
            })
        );

        return;
    }

    QString body = "{\"report\":[";
    for(int i = 0; i < events.size(); i++) {
        bool s = events[i].startsWith('{');
        bool e = events[i].endsWith('}');
        QString m = events[i].mid(1, events[i].length() - 2);
        bool mo = (m.indexOf('{') == -1);
        bool mc = (m.indexOf('}') == -1);
        if(s && e && mo && mc) {
            body += events[i];
            if (i < events.size() - 1) body += ", ";
        }
    }
    body += "]}";

    qDebug() << "Event report: " << body << "\n";

    eventReported = events.size();

    QJsonDocument doc(QJsonDocument::fromJson(body.toUtf8()));
    QJsonObject args = doc.object();

    QMetaObject::invokeMethod(&connection, "call",
      Qt::QueuedConnection,
      Q_ARG(QWebSocket*, &connection.m_webSocket),
      Q_ARG(QString, "control"),
      Q_ARG(QString, "terminalEvent"),
      Q_ARG(QJsonObject, args),
      Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
        [&](QWebSocket *connection, QJsonObject args) {
            eventReport.removeFirst(eventReported);

            eventReportTime = timeCorrection.getLocalTime();
        }),
     Q_ARG(std::function<void(QWebSocket *connection, QJsonObject args)>,
        [&](QWebSocket *connection, QJsonObject args) {
            qDebug() << "event report error: " << args["error"].toString();

            eventReportTime = timeCorrection.getLocalTime();
        })
    );
}

void Core::doFileUpload() {
    if(uploadState != OperationState::OperationStateUnknown) return;
    if(config.uploadServerUrl == "") return;

    //qDebug() << "doFileUpload";

    QDir dir(STORAGE_FOLDER);

    QStringList filters;
    filters << "*.ready";
    dir.setNameFilters(filters);
    dir.setSorting(QDir::Name);

    QStringList files = dir.entryList();
    if(files.size() == 0) return;

    QFileInfo fi(STORAGE_FOLDER + files[0]);

    QStringList parts = files[0].split('-');
    QString path = fi.absoluteFilePath();
    QString type = "" + parts[4][0];
    QString index = parts[3];

    uploadState = OperationState::OperationStatePending;

    emit uploadFile(config.serial, type, index, path, files[0], config.uploadServerUrl);
}

void Core::downloadFile(QString url, QString path) {
    DownloadRequest *request = new DownloadRequest;
    connect(request, SIGNAL(onDownloadError(DownloadRequest*, QString)), this, SLOT(onDownloadError(DownloadRequest*, QString)));
    connect(request, SIGNAL(onDownloadComplete(DownloadRequest*)), this, SLOT(onDownloadComplete(DownloadRequest*)));
    request->execute(QUrl(url), path);
}

void Core::onDownloadError(DownloadRequest *request, QString message) {
    qDebug() << message << "\n";

    addEventReport(createMessageEvent(message));

    DownloadRequest::clean(request);
}

void Core::onDownloadComplete(DownloadRequest *request) {
    qDebug() << "file downloaded";
    addEventReport(createMessageEvent("Download successfull: "));
    DownloadRequest::clean(request);

    QDir updateFolder(UPDATE_TEMP_DIR);
    updateFolder.removeRecursively();
    updateFolder.mkdir(UPDATE_TEMP_DIR);

    QProcess *p = new QProcess(nullptr);
    QString unzipCmd = QString("unzip ") + UPDATE_STORAGE + " -d " + UPDATE_TEMP_DIR;
    p->start(unzipCmd);
    p->waitForFinished();
    qDebug() << "unzip: " << p->errorString();
    p->deleteLater();

    stop();

    qint64 pid = QCoreApplication::applicationPid();

    QProcess *rp = new QProcess(nullptr);
    QString updateCmd = QString(UPDATE_SCRIPT) + " " + QString::number(pid);
    rp->startDetached(updateCmd);
    qDebug() << "restart: " << rp->errorString();
    rp->deleteLater();

    QApplication::quit();
}
