#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "terminalconfig.h"
#include "serverconnection.h"
#include "asyncoperationstate.h"
#include "core.h"
#include "channelwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Core core;

    ChannelWidget channels[4];

    QNetworkAccessManager *networkManager;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_settingsBtn_clicked();
    void showFrame(char *data, int width, int height, int stride, int displayIdx);
    void uploadFile(QString terminalSerial, QString fileType, QString channelIdx, QString path, QString fileName, QString sUrl);

    void uploadError();
    void uploadDone();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
