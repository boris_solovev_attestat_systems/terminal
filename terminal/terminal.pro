#-------------------------------------------------
#
# Project created by QtCreator 2018-04-25T20:16:13
#
#-------------------------------------------------

QT       += core gui websockets widgets multimedia network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = terminal
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_NO_FOREACH

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# WINDOWS
INCLUDEPATH += \
    ../lib/librealsense/include \
    ../lib/OCV/Win/opencv/build/include
LIBS += \
    -L"..\Lib\OCV\Win\opencv\build\x64\vc14\lib" \
    -L"..\Lib\librealsense\build\Debug" \
    -lopencv_world310d \
    -lrealsense2

# XENIAL
# INCLUDEPATH += /usr/local/include/opencv
# LIBS += \
#     -L/usr/local/lib \
#     -lcrypto \
#     -lssl \
#     -lrealsense2 \
#     -lopencv_core \
#     -lopencv_imgcodecs \
#     -lopencv_videoio \
#     -lopencv_imgproc \
#     -lopencv_features2d \
#     -lopencv_objdetect \
#     -lopencv_highgui

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    serverconnection.cpp \
    terminalconfig.cpp \
    asyncoperationstate.cpp \
    core.cpp \
    settingsdialog.cpp \
    filedownloader.cpp \
    channelwidget.cpp \
    realsensecamera.cpp

HEADERS += \
        mainwindow.h \
    serverconnection.h \
    terminalconfig.h \
    asyncoperationstate.h \
    core.h \
    settingsdialog.h \
    filedownloader.h \
    channelwidget.h \
    realsensecamera.h \
    ObjectCounter.h \
    defines.h

FORMS += \
        mainwindow.ui \
    settingsdialog.ui
