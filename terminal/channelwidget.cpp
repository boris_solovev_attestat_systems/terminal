#include "channelwidget.h"

ChannelWidget::ChannelWidget(QWidget *parent) : QWidget(parent)
{
    init();
}

void ChannelWidget:: init() {
    QGridLayout *form =  new QGridLayout();

    cameraImage = new QLabel();
    cameraImage->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    cameraImage->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );

    form->addWidget(cameraImage, 0, 0, 1, 4);

    serialTxt = new QLabel;
    serialTxt->setText("Серийный номер:");
    form->addWidget(serialTxt, 1, 0);
    serialEdt = new QLineEdit;
    serialEdt->setReadOnly(true);
    form->addWidget(serialEdt, 1, 1);

    modelTxt = new QLabel;
    modelTxt->setText("Модель:");
    form->addWidget(modelTxt, 2, 0);
    modelEdt = new QLineEdit;
    modelEdt->setReadOnly(true);
    form->addWidget(modelEdt, 2, 1);

    recordVideoCheck = new QCheckBox;
    recordVideoCheck->setText("Писать видео");
    form->addWidget(recordVideoCheck, 4, 0, 1, 2);

    recordDepthCheck = new QCheckBox;
    recordDepthCheck->setText("Писать глубину");
    form->addWidget(recordDepthCheck, 5, 0, 1, 2);

    doAnalyzeCheck = new QCheckBox;
    doAnalyzeCheck->setText("Вести анализ");
    form->addWidget(doAnalyzeCheck, 6, 0, 1, 2);

    indexTxt =  new QLabel;
    indexTxt->setText("Номер:");
    form->addWidget(indexTxt, 1, 2);
    indexEdt = new QLineEdit;
    form->addWidget(indexEdt, 1, 3);

    doorTxt =  new QLabel;
    doorTxt->setText("Двер:");
    form->addWidget(doorTxt, 2, 2);
    doorEdt = new QLineEdit;
    form->addWidget(doorEdt, 2, 3);

    highTxt =  new QLabel;
    highTxt->setText("Верхняя граница:");
    form->addWidget(highTxt, 3, 2);
    highEdt = new QLineEdit;
    form->addWidget(highEdt, 3, 3);

    lowTxt =  new QLabel;
    lowTxt->setText("Нижняя граница:");
    form->addWidget(lowTxt, 4, 2);
    lowEdt = new QLineEdit;
    form->addWidget(lowEdt, 4, 3);

    sizeTxt =  new QLabel;
    sizeTxt->setText("Размер объекта:");
    form->addWidget(sizeTxt, 5, 2);
    sizeEdt = new QLineEdit;
    form->addWidget(sizeEdt, 5, 3);

    showDepthCheck = new QCheckBox;
    showDepthCheck->setText("Глубина");
    form->addWidget(showDepthCheck, 6, 2);

    showInfoCheck = new QCheckBox;
    showInfoCheck->setText("Инфо");
    form->addWidget(showInfoCheck, 6, 3);

    showSettingsBtn = new QPushButton;
    showSettingsBtn->setText("Показать настройки");
    form->addWidget(showSettingsBtn, 8, 0, 1, 4);

    saveBtn = new QPushButton;
    saveBtn->setText("Сохранить");
    form->addWidget(saveBtn, 7, 0, 1, 2);

    cancelBtn = new QPushButton;
    cancelBtn->setText("Отменить");
    form->addWidget(cancelBtn, 7, 2, 1, 2);

    form->setColumnStretch(0, 1);
    form->setColumnStretch(1, 1);
    form->setColumnStretch(2, 1);
    form->setColumnStretch(3, 1);

    cancel();

    setLayout(form);

    connect(showSettingsBtn, SIGNAL(clicked(bool)), this, SLOT(showSettings()));
    connect(saveBtn, SIGNAL(clicked(bool)), this, SLOT(save()));
    connect(cancelBtn, SIGNAL(clicked(bool)), this, SLOT(cancel()));
}

void ChannelWidget::showSettings() {
    if(configIndex == -1) return;

    for(int ci = 0; ci < 4; ci++) {
        if(config->channels[ci].index == configIndex) {
            recordVideoCheck->setChecked(config->channels[ci].recordVideo == 1);
            recordDepthCheck->setChecked(config->channels[ci].recordDepth == 1);
            doAnalyzeCheck->setChecked(config->channels[ci].doAnalyze == 1);

            serialEdt->setText(config->channels[ci].serial);
            modelEdt->setText(config->channels[ci].model);
            indexEdt->setText(QString::number(config->channels[ci].index));
            doorEdt->setText(config->channels[ci].door);
            lowEdt->setText(QString::number(config->channels[ci].lowLinePos));
            highEdt->setText(QString::number(config->channels[ci].highLinePos));
            sizeEdt->setText(QString::number(config->channels[ci].objectSizeLimit));

            showDepthCheck->setChecked(config->channels[ci].showDepth);
            showInfoCheck->setChecked(config->channels[ci].showInfo);

            showSettings(true);

            break;
        }
    }

    qDebug() << "Configs:";
    for(int i = 0; i < 4; i++) {
        qDebug() << "Model:" << config->channels[i].model;
        qDebug() << "Serial:" << config->channels[i].serial;
        qDebug() << "Index:" << config->channels[i].index;
        qDebug() << "ShowDepth:" << config->channels[i].showDepth;
    }
}

void ChannelWidget::save() {
    for(int ci = 0; ci < 4; ci++) {
        if(config->channels[ci].index == configIndex) {
            config->channels[ci].recordVideo = recordVideoCheck->isChecked()?1:0;
            config->channels[ci].recordDepth = recordDepthCheck->isChecked()?1:0;
            config->channels[ci].doAnalyze = doAnalyzeCheck->isChecked()?1:0;

            config->channels[ci].serial = serialEdt->text();
            config->channels[ci].model = modelEdt->text();
            config->channels[ci].index = indexEdt->text().toInt();
            config->channels[ci].door = doorEdt->text();
            config->channels[ci].lowLinePos = lowEdt->text().toInt();
            config->channels[ci].highLinePos = highEdt->text().toInt();
            config->channels[ci].objectSizeLimit = sizeEdt->text().toInt();

            config->channels[ci].showDepth = showDepthCheck->isChecked();
            config->channels[ci].showInfo = showInfoCheck->isChecked();

            config->save();

            break;
        }
    }
    qDebug() << "Configs:";
    for(int i = 0; i < 4; i++) {
        qDebug() << "Model:" << config->channels[i].model;
        qDebug() << "Serial:" << config->channels[i].serial;
        qDebug() << "Index:" << config->channels[i].index;
        qDebug() << "ShowDepth:" << config->channels[i].showDepth;
    }

    showSettings(false);
}

void ChannelWidget::cancel() {
    showSettings(false);
}

void ChannelWidget::showSettings(bool show) {
    if(show) {
        showSettingsBtn->hide();
        saveBtn->show();
        cancelBtn->show();

        recordVideoCheck->show();
        recordDepthCheck->show();
        doAnalyzeCheck->show();

        serialTxt->show();
        modelTxt->show();
        indexTxt->show();
        doorTxt->show();
        lowTxt->show();
        highTxt->show();
        sizeTxt->show();

        serialEdt->show();
        modelEdt->show();
        indexEdt->show();
        doorEdt->show();
        lowEdt->show();
        highEdt->show();
        sizeEdt->show();

        showDepthCheck->show();
        showInfoCheck->show();
    } else {
        showSettingsBtn->show();
        saveBtn->hide();
        cancelBtn->hide();

        recordVideoCheck->hide();
        recordDepthCheck->hide();
        doAnalyzeCheck->hide();

        serialTxt->hide();
        modelTxt->hide();
        indexTxt->hide();
        doorTxt->hide();
        lowTxt->hide();
        highTxt->hide();
        sizeTxt->hide();

        serialEdt->hide();
        modelEdt->hide();
        indexEdt->hide();
        doorEdt->hide();
        lowEdt->hide();
        highEdt->hide();
        sizeEdt->hide();

        showDepthCheck->hide();
        showInfoCheck->hide();
    }
}
