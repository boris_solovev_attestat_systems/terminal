#ifndef ASYNCOPERATIONSTATE_H
#define ASYNCOPERATIONSTATE_H

#include <mutex>
#include <QDateTime>

class AsyncOperationState
{
public:
    std::mutex stateCS;
    enum OperaionState {
        NOT_PERFORMED,
        PENDING,
        PERFORMED
    };
    OperaionState state = OperaionState::NOT_PERFORMED;
    qint64 startTime;
    int currentTimeout;

    AsyncOperationState();

    OperaionState getState();
    OperaionState _getState();
    void _setState(OperaionState state, int timeout = 3000);
};

#endif // ASYNCOPERATIONSTATE_H
