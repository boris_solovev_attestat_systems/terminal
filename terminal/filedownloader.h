#ifndef FILEDOWNLOADER_H
#define FILEDOWNLOADER_H

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QThread>

class FileDownloader : public QObject
{
    Q_OBJECT

    QUrl url;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    QFile *file;
    bool requestAborted;
public:
    void startDownload(QUrl url, QString fileName) {
        QSslConfiguration sslConf = QSslConfiguration::defaultConfiguration();
        sslConf.setPeerVerifyMode(QSslSocket::VerifyNone);
        QSslConfiguration::setDefaultConfiguration(sslConf);

        manager = new QNetworkAccessManager(this);

        if (QFile::exists(fileName)) {
                QFile::remove(fileName);
        }

        file = new QFile(fileName);
        if (!file->open(QIODevice::WriteOnly)) {
            emit error(this, QString("Unable to save the file %1: %2.").arg(fileName).arg(file->errorString()));
            delete file;
            file = 0;
            return;
        }

        startRequest(url);
    }

    void startRequest(QUrl url) {
        reply = manager->get(QNetworkRequest(url));

        connect(reply, SIGNAL(readyRead()), this, SLOT(readyRead()));
        connect(reply, SIGNAL(finished()), this, SLOT(downloadFinished()));
    }

private slots:
    void readyRead() {
        QByteArray data = reply->readAll();

        if(data.size() > 0)
            if (file) file->write(data);
    }

    void downloadFinished() {
        deleteLater();

        file->flush();
        file->close();

        QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
        if (reply->error()) {
            file->remove();
            emit error(this, QString("Download failed: %1.").arg(reply->errorString()));
        } else if (!redirectionTarget.isNull()) {
            QUrl newUrl = url.resolved(redirectionTarget.toUrl());
            url = newUrl;
            reply->deleteLater();
            file->open(QIODevice::WriteOnly);
            file->resize(0);
            startRequest(url);
            return;
        } else {
            emit complete(this);
        }

        reply->deleteLater();
        reply = 0;
        delete file;
        file = 0;
        manager = 0;
    }

signals:
    void error(FileDownloader *downloader, QString message);
    void complete(FileDownloader *downloader);
};

class DownloadRequest : public QObject
{
    Q_OBJECT

    QThread thread;
public:
    DownloadRequest() {
        moveToThread(&thread);
        FileDownloader *worker = new FileDownloader;
        worker->moveToThread(&thread);
        connect(&thread, &QThread::finished, worker, &QObject::deleteLater);
        connect(this, &DownloadRequest::execute, worker, &FileDownloader::startDownload);
        connect(worker, &FileDownloader::complete, this, &DownloadRequest::complete);
        connect(worker, &FileDownloader::error, this, &DownloadRequest::error);
        thread.start();
    }

    static void clean(DownloadRequest *request) {
        request->thread.quit();
        request->thread.wait();
        delete request;
    }

signals:
    void execute(QUrl url, QString path);
    void onDownloadError(DownloadRequest *request, QString message);
    void onDownloadComplete(DownloadRequest *request);
public slots:
    void error(FileDownloader *, QString message) {
        emit onDownloadError(this, message);
    }

    void complete(FileDownloader *) {
        emit onDownloadComplete(this);
    }
};


#endif // FILEDOWNLOADER_H
