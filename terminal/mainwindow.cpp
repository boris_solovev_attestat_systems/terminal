#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    channels[0].config = &core.config;
    channels[0].configIndex = 0;
    channels[1].config = &core.config;
    channels[1].configIndex = 1;
    channels[2].config = &core.config;
    channels[2].configIndex = 2;
    channels[3].config = &core.config;
    channels[3].configIndex = 3;

    ui->gridLayout->addWidget(&channels[0], 0, 0);
    ui->gridLayout->addWidget(&channels[1], 0, 1);
    ui->gridLayout->addWidget(&channels[2], 1, 0);
    ui->gridLayout->addWidget(&channels[3], 1, 1);

    connect(&core, SIGNAL(showFrame(char*, int, int, int, int)), this, SLOT(showFrame(char*, int, int, int, int)));
    connect(&core, SIGNAL(uploadFile(QString,QString,QString,QString,QString,QString)), this, SLOT(uploadFile(QString,QString,QString,QString,QString,QString)));

    core.start();

    networkManager= new QNetworkAccessManager;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_settingsBtn_clicked()
{
    SettingsDialog *dlg = new SettingsDialog(&core.config, this);

    dlg->exec();
}

void MainWindow::showFrame(char *data, int width, int height, int stride, int displayIdx) {
    if(displayIdx >= 4 || displayIdx < 0) return;

    QImage image((uchar*)data, width, height, stride, QImage::Format_RGB888);
    QPixmap qPixMap = QPixmap::fromImage(image);
    channels[displayIdx].cameraImage->setPixmap(
                qPixMap.scaled(
                    channels[displayIdx].cameraImage->width(),
                    channels[displayIdx].cameraImage->height(),
                    Qt::KeepAspectRatio
                    )
                );

    delete[] data;
}

void MainWindow::uploadFile(QString terminalSerial, QString fileType, QString channelIdx, QString path, QString fileName, QString sUrl) {
    core.currentUploadPath = path;
    core.currentUploadType = fileType;
    core.currentUploadChannel = core.config.channels[channelIdx.toInt()].serial;


    QJsonObject e;
    e["type"] = "uploadStart";
    e["data"] = core.currentUploadPath;
    e["path"] = core.currentUploadPath;
    e["type"] = core.currentUploadType;
    e["channel"] = core.currentUploadChannel;
    core.addEventReport(e);

    QUrl url(sUrl);
    QNetworkRequest request(url);

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"file\"; filename=\"" + fileName + "\""));

    QHttpPart serialPart;
    serialPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"terminal_serial\""));
    serialPart.setBody(terminalSerial.toUtf8());
    QHttpPart typePart;
    typePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"file_type\""));
    typePart.setBody(fileType.toUtf8());
    QHttpPart indexPart;
    indexPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"channel_index\""));
    indexPart.setBody(channelIdx.toUtf8());

    core.currentUploadfile = new QFile(path);
    core.currentUploadfile->open(QIODevice::ReadOnly);

    imagePart.setBodyDevice(core.currentUploadfile);

    multiPart->append(serialPart);
    multiPart->append(typePart);
    multiPart->append(indexPart);
    multiPart->append(imagePart);

    core.currentUploadReply = networkManager->post(request, multiPart);
    multiPart->setParent(core.currentUploadReply);

    connect(core.currentUploadReply, SIGNAL(finished()), this, SLOT  (uploadDone()));
    connect(core.currentUploadReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT  (uploadError()));
}

void MainWindow::uploadDone() {
    if(core.currentUploadReply == nullptr) {
        core.uploadState = Core::OperationState::OperationStateUnknown;
        return;
    }

    qDebug() << "uploadDone";
    core.currentUploadReply->deleteLater();

    core.currentUploadfile->close();
    delete core.currentUploadfile;

    try {
        QFile file (core.currentUploadPath);
        core.config.addStorageSize(-file.size());
        file.remove();
    } catch(...) {
        qDebug() << "Remove failed";
    }

    core.currentUploadReply = nullptr;

    core.addEventReport(core.createMessageEvent("File " + core.currentUploadPath + " uploaded"));
    QJsonObject e;
    e["type"] = "storage_size";
    e["data"] = core.config.storageSize;
    core.addEventReport(e);

    QJsonObject e2;
    e2["type"] = "uploadDone";
    e2["data"] = core.currentUploadPath;
    e2["path"] = core.currentUploadPath.mid(0, core.currentUploadPath.length() - 6);
    e2["file_type"] = core.currentUploadType;
    e2["channel"] = core.currentUploadChannel;
    core.addEventReport(e2);

    core.uploadState = Core::OperationState::OperationStateUnknown;
}

void MainWindow::uploadError() {
    if(core.currentUploadReply == nullptr) {
        core.uploadState = Core::OperationState::OperationStateUnknown;
        return;
    }

    qDebug() << "uploadError" << core.currentUploadReply->errorString() << " " << core.currentUploadReply->error();
    QString message = QString("File ") + core.currentUploadPath + QString(" upload failed: ") + core.currentUploadReply->errorString();
    message = message.remove("\n").remove("\r");
    //core.addEventReport(core.createMessageEvent(message));

    core.currentUploadReply->deleteLater();
    core.currentUploadReply = nullptr;

    core.currentUploadfile->close();
    delete core.currentUploadfile;

    QJsonObject e;
    e["type"] = "uploadError";
    e["data"] = core.currentUploadPath;
    e["path"] = core.currentUploadPath;
    e["file_type"] = core.currentUploadType;
    e["channel"] = core.currentUploadChannel;
    core.addEventReport(e);

    core.uploadState = Core::OperationState::OperationStateUnknown;
}
