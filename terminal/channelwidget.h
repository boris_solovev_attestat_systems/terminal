#ifndef CHANNELWIDGET_H
#define CHANNELWIDGET_H

#include <QObject>
#include <QWidget>
#include <QtWidgets>
#include <QFormLayout>
#include "terminalconfig.h"

class ChannelWidget : public QWidget
{
    Q_OBJECT

    QCheckBox *recordVideoCheck;
    QCheckBox *recordDepthCheck;
    QCheckBox *doAnalyzeCheck;

    QCheckBox *showDepthCheck;
    QCheckBox *showInfoCheck;

    QLabel *serialTxt;
    QLabel *modelTxt;
    QLabel *indexTxt;
    QLabel *doorTxt;
    QLabel *lowTxt;
    QLabel *highTxt;
    QLabel *sizeTxt;

    QLineEdit *serialEdt;
    QLineEdit *modelEdt;
    QLineEdit *indexEdt;
    QLineEdit *doorEdt;
    QLineEdit *lowEdt;
    QLineEdit *highEdt;
    QLineEdit *sizeEdt;

    QPushButton *showSettingsBtn;
    QPushButton *saveBtn;
    QPushButton *cancelBtn;

    void init();
public:
    QLabel *cameraImage;

    TerminalConfig *config;
    int configIndex = -1;

    explicit ChannelWidget(QWidget *parent = nullptr);

    void showSettings(bool show);

signals:

public slots:
    void showSettings();
    void save();
    void cancel();
};

#endif // CHANNELWIDGET_H
