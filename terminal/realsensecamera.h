#ifndef REAKSENSECAMERA_H
#define REAKSENSECAMERA_H

#include <mutex>
#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>
#include "ObjectCounter.h"
#include <iostream>
#include <fstream>

#include "defines.h"

using namespace std;
using namespace std::chrono;

class RealSenseCamera;

class CameraListener {
public:
    virtual void onVideoFrame(cv::Mat &mat, RealSenseCamera *source) = 0;
    virtual void onDepthFrame(cv::Mat &mat, RealSenseCamera *source) = 0;
    virtual void onFileReady(string path, string type, int configIndex) = 0;
    virtual void onNewFile(string path, string type, int configIndex) = 0;
};

class RealSenseCamera
{
    int sourceWidth = 320;
    int sourceHeight = 240;

    rs2::config cfg;
    rs2::pipeline p;

    rs2::device device;

    std::ofstream passFile;
    cv::VideoWriter *depthWriter = nullptr;
    cv::VideoWriter *videoWriter = nullptr;
    //system_clock::time_point depthWriterStart;
    //system_clock::time_point videoWriterStart;
    string videoFilePath;
    string depthFilePath;
    string passFilePath;

    int depthFrame = 0;
    int videoFrame = 0;

    string joinPath(string s1, string s2);

    string time_point_to_string(system_clock::time_point &tp);
    void writeVideoFrame(cv::Mat *frame);
    void writeDepthFrame(cv::Mat *frame);
public:
    HeadCounter::ObjectCounter *counter;

    cv::Mat gsSrc;

    string terminalId;
    string serial;
    string model;
    int configIndex = -1;

    CameraListener *listener;

    RealSenseCamera();
    ~RealSenseCamera();

    void init(rs2::device device);
    void open();
    void handleFrames(bool recordVideo, bool recordDepth, bool analize, bool reverse);
    void close();

    void closeVideoWriter();
    void closeDepthWriter();
};

#endif // REAKSENSECAMERA_H
