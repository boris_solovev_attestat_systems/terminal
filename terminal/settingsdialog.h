#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "terminalconfig.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(TerminalConfig *terminalConfig, QWidget *parent = 0);
    ~SettingsDialog();

    TerminalConfig *terminalConfig;
private:
    Ui::SettingsDialog *ui;

private slots:
    void saveConfig();
};

#endif // SETTINGSDIALOG_H
