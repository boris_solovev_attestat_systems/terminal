#include "realsensecamera.h"
#include <opencv2/opencv.hpp>

#include <stdlib.h>
#include <sys/stat.h>
//#include <direct.h>

RealSenseCamera::RealSenseCamera() {
}

RealSenseCamera::~RealSenseCamera() {
    close();
}

void RealSenseCamera::init(rs2::device device) {
    serial = device.get_info(rs2_camera_info::RS2_CAMERA_INFO_SERIAL_NUMBER);
    model = device.get_info(rs2_camera_info::RS2_CAMERA_INFO_NAME);
    this->device = device;

    /*vector<rs2::sensor> sensors = device.query_sensors();

    for(int i = 0; i < sensors.size(); i++) {
        sensors[i].set_option(rs2_option::);
    }*/
}

void RealSenseCamera::open() {
    printf("Open camera: %s (%s)\n", model.c_str(), serial.c_str());

    cfg.enable_device(serial);
    cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 30);
    cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_RGB8, 30);

    try {
        p.start(cfg);
    }
    catch (...) {
        printf("Cannot initialize camera %s \n", serial.c_str());
        exit(0);
    }

    counter = new HeadCounter::ObjectCounter(
        // invoked if a passenger have passed in
        [&](shared_ptr<HeadCounter::Object> obj) {
        },
        // invoked if a passenger have passed out
        [&](shared_ptr<HeadCounter::Object> obj) {
        }
    );
}

void RealSenseCamera::handleFrames(bool recordVideo, bool recordDepth, bool analize, bool reverse) {
    rs2::frameset frames = p.wait_for_frames();
    if (frames.size() == 0) return;

    for (int i = 0; i < frames.size(); i++) {
        rs2::depth_frame depth = frames.get_depth_frame();
        if (depth.get_width() != 0) {
            int imageDataSize = depth.get_height() * depth.get_width();
            char *imageData = (char *)malloc(imageDataSize);
            uint16_t *depthData = (uint16_t *)depth.get_data();

            for (int j = 0; j < imageDataSize; j++) {
                double v = depthData[j] / 10.0;

                if (v > 255) v = 255;

                imageData[j] = (char)v;
            }
            cv::Mat image(cv::Size(640, 480), CV_8UC1, imageData, cv::Mat::AUTO_STEP);
            cv::resize(image, image, cv::Size(sourceWidth, sourceHeight));

            //cv::Mat gsSrc;
            gsSrc.release();
            cv::cvtColor(image, gsSrc, CV_GRAY2BGR);
            free(imageData);

            if(listener) listener->onDepthFrame(gsSrc, this);

            int in_check = counter->cnt_in;
            int out_check = counter->cnt_out;
            if(analize) counter->count(image, false, true, &gsSrc, reverse);

            if (recordDepth) {
                writeDepthFrame(&image);

                if(in_check != counter->cnt_in) passFile << depthFrame << "\tin\t" << (counter->cnt_in - in_check) << "\n";
                if(out_check != counter->cnt_out) passFile << depthFrame << "\tout\t" << (counter->cnt_out - out_check) << "\n";
            } else {
                if (depthWriter != nullptr) {
                    closeDepthWriter();
                }
            }

            image.release();
        }

        rs2::video_frame video = frames.get_color_frame();
        if (video.get_width() != 0) {
            int w = video.get_stride_in_bytes();
            int h = video.get_height();
            uint8_t *videoData = (uint8_t *)video.get_data();

            cv::Mat image(cv::Size(640, 480), CV_8UC3, videoData, cv::Mat::AUTO_STEP);

            if(listener) listener->onVideoFrame(image, this);

            if(recordVideo) {
                writeVideoFrame(&image);
            } else {
                if (videoWriter != nullptr) closeVideoWriter();
            }

            image.release();
        }
    }
}

void RealSenseCamera::close() {
    p.stop();
}

string RealSenseCamera::joinPath(string s1, string s2) {
    if (s1[s1.length() - 1] != '/') s1 += '/';

    return s1 + s2;
}

string RealSenseCamera::time_point_to_string(system_clock::time_point &tp)
{
    auto ttime_t = system_clock::to_time_t(tp);
    auto tp_sec = system_clock::from_time_t(ttime_t);
    milliseconds ms = duration_cast<milliseconds>(tp - tp_sec);

    std::tm * ttm = localtime(&ttime_t);

    char date_time_format[] = "%Y%m%dT%H%M%S";
    char time_str[] = "YYYYmmddTHHMMSS-uuu-nucid-i";
    strftime(time_str, strlen(time_str), date_time_format, ttm);

    string result(time_str);
    result.append("-");
    result.append(to_string(ms.count()));
    result.append("-");
    result.append(terminalId);

    result.append("-");
    result.append(to_string(configIndex));

    return result;
}

void RealSenseCamera::writeVideoFrame(cv::Mat *frame) {
    //auto timeDiff = std::chrono::duration_cast<std::chrono::seconds>(system_clock::now() - videoWriterStart);
    //if (timeDiff.count() >= (3 * 60) && videoWriter != nullptr) closeVideoWriter();
    if(videoFrame >= 10800) closeVideoWriter();

    if (videoWriter == nullptr) {
        //string folderName = joinPath(VIDEO_FOLDER, to_string(configIndex));
        //mkdir(folderName.c_str());

        system_clock::time_point videoWriterStart = system_clock::now();
        videoFilePath = joinPath(STORAGE_FOLDER, time_point_to_string(videoWriterStart)) + "-v" + ".avi";

        videoWriter = new cv::VideoWriter(videoFilePath, cv::VideoWriter::fourcc('M', 'P', '4', '2'), 30, cv::Size(frame->cols, frame->rows), true);

        listener->onNewFile(videoFilePath, "v", configIndex);
    }

    videoWriter->write(*frame);
    videoFrame++;
}

void RealSenseCamera::closeVideoWriter() {
    videoFrame = 0;
    videoWriter->release();
    delete videoWriter;
    videoWriter = nullptr;

    string newVideoName = videoFilePath + ".ready";
    rename(videoFilePath.c_str(), newVideoName.c_str());

    listener->onFileReady(newVideoName, "v", configIndex);
}

void RealSenseCamera::writeDepthFrame(cv::Mat *frame) {
    //auto timeDiff = std::chrono::duration_cast<std::chrono::seconds>(system_clock::now() - depthWriterStart);
    //if (timeDiff.count() >= (3 * 60) && depthWriter != nullptr) closeDepthWriter();

    if(depthFrame >= 10800) closeDepthWriter();

    if (depthWriter == nullptr) {
        //string folderName = joinPath(DEPTH_FOLDER, to_string(configIndex));
        //mkdir(folderName.c_str());

        system_clock::time_point depthWriterStart = system_clock::now();
        depthFilePath = joinPath(STORAGE_FOLDER, time_point_to_string(depthWriterStart)) + "-d" + ".avi";
        depthWriter = new cv::VideoWriter(depthFilePath, cv::VideoWriter::fourcc('M', 'P', '4', '2'), 30, cv::Size(frame->cols, frame->rows), false);

        passFilePath = joinPath(STORAGE_FOLDER, time_point_to_string(depthWriterStart)) + "-p" + ".pass";
        passFile.open(passFilePath);

        listener->onNewFile(depthFilePath, "d", configIndex);
        listener->onNewFile(passFilePath, "p", configIndex);
    }

    depthWriter->write(*frame);
    depthFrame++;
}

void RealSenseCamera::closeDepthWriter() {
    this->depthFrame = 0;

    passFile.flush();
    passFile.close();

    depthWriter->release();
    delete depthWriter;
    depthWriter = nullptr;

    string newDepthName = depthFilePath + ".ready";
    string newPassName = passFilePath + ".ready";

    rename(depthFilePath.c_str(), newDepthName.c_str());
    rename(passFilePath.c_str(), newPassName.c_str());

    listener->onFileReady(newDepthName, "d", configIndex);
    listener->onFileReady(newPassName, "p", configIndex);
}
