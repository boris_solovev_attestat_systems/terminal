#ifndef TERMINALCONFIG_H
#define TERMINALCONFIG_H

#include <QString>
#include <QSettings>
#include <mutex>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStorageInfo>

#include "defines.h"

class TerminalConfig
{
public:
    class ChannelConfig {
    public:
        QString serial;
        QString model;
        int lowLinePos;
        int highLinePos;
        int objectSizeLimit;

        bool recordVideo;
        bool recordDepth;
        bool doAnalyze;

        int index;
        QString door;

        bool showDepth = false;
        bool showInfo = false;

        QAtomicInt videoStreamClients = 0;
        QAtomicInt depthStreamClients = 0;
        QAtomicInt infoStreamClients = 0;

        QAtomicInt s_videoStreamClients = 0;
        QAtomicInt s_depthStreamClients = 0;
        QAtomicInt s_infoStreamClients = 0;

        int lowThreshold;
        int highThreshold;
        int erosion_size;
        double sNear;
        int maxObjectFrames;
        int reverse;
    };

    bool changed = false;

    QString serial;
    QString software;
    QString secret;
    QString serverUrl;
    QString uploadServerUrl;
    int syncTime;

    long long startStorageLimit;
    long long stopStorageLimit;

    ChannelConfig channels[4];

    std::mutex storageSizeCS;
    long long storageSize = 0;

    long long addStorageSize(long long value) {
        std::lock_guard<std::mutex> lock(storageSizeCS);
        storageSize += value;

        return storageSize;
    }

    bool storageStartLimitReached() {
        std::lock_guard<std::mutex> lock(storageSizeCS);

        QStorageInfo diskInfo = QStorageInfo::root();

        qint64 free = diskInfo.bytesFree();
        return (free <= startStorageLimit);
    }

    bool storageStopLimitReached() {
        std::lock_guard<std::mutex> lock(storageSizeCS);

        QStorageInfo diskInfo = QStorageInfo::root();
        return (diskInfo.bytesFree() <= stopStorageLimit);
    }

    void load() {
        serial = "";
        software = "0.0.7";
        secret = "";
        serverUrl = "";
        uploadServerUrl = "";
        syncTime = 60;

        startStorageLimit = 20LL * 1024LL * 1024LL * 1024LL;
        stopStorageLimit = 2LL * 1024LL * 1024LL * 1024LL;

        for(int i = 0; i < 4; i++) {
            channels[i].door = "";
            channels[i].index = -1;
            channels[i].serial = "";
            channels[i].lowLinePos = 70;
            channels[i].highLinePos = 110;
            channels[i].objectSizeLimit = 20;

            channels[i].recordVideo = false;
            channels[i].recordDepth = false;
            channels[i].doAnalyze = false;

            channels[i].lowThreshold = 5;
            channels[i].highThreshold = 80;
            channels[i].erosion_size = 5;
            channels[i].sNear = 20;
            channels[i].maxObjectFrames = 6;
            channels[i].reverse = 0;
        }

        QSettings settings("attistat", "terminal");

        serial = settings.value("serial", serial).toString();
        //software = settings.value("software", software).toString();
        secret= settings.value("secret", secret).toString();
        serverUrl = settings.value("serverUrl", serverUrl).toString();
        uploadServerUrl = settings.value("uploadServerUrl", uploadServerUrl).toString();
        syncTime = settings.value("syncTime", syncTime).toInt();

        startStorageLimit = settings.value("startStorageLimit", startStorageLimit).toLongLong();
        stopStorageLimit = settings.value("stopStorageLimit", stopStorageLimit).toLongLong();

        for(int i = 0; i < 4; i++) {
            channels[i].door = settings.value("door_" + QString::number(i), channels[i].door).toString();
            channels[i].index = settings.value("index_" + QString::number(i), channels[i].index).toInt();
            channels[i].serial = settings.value("serial_" + QString::number(i), channels[i].serial).toString();
            channels[i].lowLinePos = settings.value("lowLinePos_" + QString::number(i), channels[i].lowLinePos).toInt();
            channels[i].highLinePos = settings.value("highLinePos_" + QString::number(i), channels[i].highLinePos).toInt();
            channels[i].objectSizeLimit = settings.value("objectSizeLimit_" + QString::number(i), channels[i].objectSizeLimit).toInt();

            channels[i].recordVideo = settings.value("recordVideo_" + QString::number(i), channels[i].recordVideo).toInt() == 1;
            channels[i].recordDepth = settings.value("recordDepth_" + QString::number(i), channels[i].recordDepth).toInt() == 1;
            channels[i].doAnalyze = settings.value("doAnalyze_" + QString::number(i), channels[i].doAnalyze).toInt() == 1;

            channels[i].lowThreshold = settings.value("lowThreshold_" + QString::number(i), channels[i].lowThreshold).toInt();
            channels[i].highThreshold = settings.value("highThreshold_" + QString::number(i), channels[i].highThreshold).toInt();
            channels[i].erosion_size = settings.value("erosion_size_" + QString::number(i), channels[i].erosion_size).toInt();
            channels[i].sNear = settings.value("sNear_" + QString::number(i), channels[i].sNear).toInt();
            channels[i].maxObjectFrames = settings.value("maxObjectFrames_" + QString::number(i), channels[i].maxObjectFrames).toInt();

            channels[i].reverse = settings.value("reverse_" + QString::number(i), channels[i].reverse).toInt();
        }
    }

    void save() {
        QSettings settings("attistat", "terminal");

        settings.setValue("serial", serial);
        settings.setValue("software", software);
        settings.setValue("secret", secret);
        settings.setValue("serverUrl", serverUrl);
        settings.setValue("uploadServerUrl", uploadServerUrl);
        settings.setValue("syncTime", syncTime);

        settings.setValue("startStorageLimit", startStorageLimit);
        settings.setValue("stopStorageLimit", stopStorageLimit);

        for(int i = 0; i < 4; i++) {
            settings.setValue("door_" + QString::number(i), channels[i].door);
            settings.setValue("index_" + QString::number(i), channels[i].index);
            settings.setValue("serial_" + QString::number(i), channels[i].serial);
            settings.setValue("lowLinePos_" + QString::number(i), channels[i].lowLinePos);
            settings.setValue("highLinePos_" + QString::number(i), channels[i].highLinePos);
            settings.setValue("objectSizeLimit_" + QString::number(i), channels[i].objectSizeLimit);

            settings.setValue("recordVideo_" + QString::number(i), channels[i].recordVideo?1:0);
            settings.setValue("recordDepth_" + QString::number(i), channels[i].recordDepth?1:0);
            settings.setValue("doAnalyze_" + QString::number(i), channels[i].doAnalyze?1:0);

            settings.setValue("lowThreshold_" + QString::number(i), channels[i].lowThreshold);
            settings.setValue("highThreshold_" + QString::number(i), channels[i].highThreshold);
            settings.setValue("erosion_size_" + QString::number(i), channels[i].erosion_size);
            settings.setValue("sNear_" + QString::number(i), channels[i].sNear);
            settings.setValue("maxObjectFrames_" + QString::number(i), channels[i].maxObjectFrames);

            settings.setValue("reverse_" + QString::number(i), channels[i].reverse);
        }

        QStringList sl = settings.allKeys();
        settings.sync();

        changed = true;
    }

    TerminalConfig();
};

#endif // TERMINALCONFIG_H
