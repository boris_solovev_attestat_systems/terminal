#include "mainwindow.h"
#include <QApplication>
#include "sigwatch.h"

void signalhandler(int sig)
{
    if (sig == SIGINT)
    {
        printf("will quit by SIGINT\n");
        qApp->quit();
    }
    else if (sig == SIGTERM)
    {
        printf("will quit by SIGTERM\n");
        qApp->quit();
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    signal(SIGINT,signalhandler);
    signal(SIGTERM,signalhandler);

    return a.exec();
}
