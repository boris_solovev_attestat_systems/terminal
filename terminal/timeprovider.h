#ifndef TIMEPROVIDER_H
#define TIMEPROVIDER_H

#include <QTime>
#include <QProcess>

class TimeProvider
{
    long long serverStartTime  = 0;
    long long localStartTime = 0;
    int timeZoneOffset = 0;

    void setSystemTime(long long utime) {
        QProcess *p = new QProcess(nullptr);
        // Change system date and time "date -u MMDDhhmmYYYY.ss" in tha application the date is created dynamically
        QString cmd = "date +%s -s @" + (qlonglong)utime;
        p->start(cmd);
        p->waitForFinished();

        // Set the Hardware Clock to the current System Time.
        p->start("hwclock -w");
        p->waitForFinished();

        p->deleteLater();
    }

public:
    bool isCorrect() {
        return (serverStartTime != 0);
    }

    void set(long long serverTime, int timeZoneOffset) {
        //setSystemTime(serverTime);

        this->serverStartTime = serverTime;
        this->localStartTime = QDateTime::currentDateTime().toTime_t();
        this->timeZoneOffset = timeZoneOffset * 60 * 60;
    }

    long long getServerTime() {
        return QDateTime::currentDateTime().toTime_t() - localStartTime + serverStartTime;
    }

    QDateTime getLocalTimeDT() {
        return QDateTime::fromTime_t(getServerTime() + timeZoneOffset);
    }

    long long getLocalTime() {
        return QDateTime::currentDateTime().toTime_t();
    }

    long long getDayTimeSec() {
        return (getServerTime() + timeZoneOffset) % (60 * 60 *24);
    }
};

#endif // TIMEPROVIDER_H
