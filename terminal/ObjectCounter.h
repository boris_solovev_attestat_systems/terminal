#pragma once

// use of standart libraries
#include <iostream>
#include <string>
#include <thread>
#include <map>

#include <opencv2/opencv.hpp>

// keep no more than 40 segments of a tracked object path
#define MAX_TRACK_LENGTH 40

// use some standart namespaces to reduce the code length
using namespace std;
//using namespace std::chrono;

// local namespace not conflict with other parts
namespace HeadCounter {

	// the tracked object class
	class Object {
	public:
		// object move direction
		enum Direction {
			UnknownDirection, // the direction still unknown - not enough information to asses it
			InsideDirection,  // object generaly moves inside the bus
			OutsideDirection  // object generally moves outside the bus
		};

		// to say if the object (human) took in or out the bus - we wait him to cross both of barriers
		// the outer barrier put closer the doors so the object crossed this barrier is taking out the bus
		// the inner one put farther the doors - so the object crosse the inner barier is definitely inside the bus
		// if an object had crossed both of barriers - we can say is he takin off/on the bus useing his direction
		// if an object crosses both the barriers but it's direction is still unknown - we deal with a noise and don't pay attention on it
		bool _innerCrossed = false; // the fact that the object already crossed the inner barier
		bool _outerCrossed = false; // the fact that the object already crossed the outer barier

		// the fact we already saw that the object cressed the bariers and was took on acount
		bool seen = false;

		// the object direction assessment
		Direction direction = Direction::UnknownDirection;

		// the last object offest on X and Y axes
		int dx = 0;
		int dy = 0;

		// the global range of the object moves on Y axis
		int minY = 1000000;
		int maxY = -1000000;

		// the object identifier just to differ it from others
		long oid;

		// frame count passed since we've seen the object last time
		long frames;

		// total frames count the object was on
		long totalFrames = 0;

		// the object path
                vector<cv::Point> tracks;
		// the last detection contour for visualization purposes
		//vector<Point> contour;

		// Constructor initializes tyhe new obejct with it's identifier and the central point
                Object(int id, cv::Point center)
		{
			oid = id;
			frames = 0;

			tracks.push_back(center);

			minY = center.y;
			maxY = center.y;
		}

		// copy the object path
                vector<cv::Point> getTracks() {
                        vector<cv::Point> track;

			for (int i = 0; i < tracks.size(); i++) track.push_back(tracks[i]);

			return track; 
		};

		// as we cannot extract the whole person at once due to technical facilities - we can obtain a human image separated in a few tracking objects
		// based on a collective movements of objects we can detect that some of them are parts of anoher bigger object
		// isLinked - checks if the object `obj` could be a part of the same composite object the current object belongs to
		//     eps  - the max offset diff we think the objects are moveing together
		//     dist - the max distance we can assess the object's linkage
		bool isLinked(std::shared_ptr<Object> obj, int eps, int dist) {
			// filter insufficient movements as these are mostly a noise
			if (abs(dx) <= eps / 2 || abs(dy) <= eps / 2) return false;
			if (abs(obj->dx) <= eps / 2 || abs(obj->dy) <= eps / 2) return false;

			// get both objects last points
                        cv::Point p0 = this->tracks[this->tracks.size() - 1];
                        cv::Point p1 = obj->tracks[obj->tracks.size() - 1];

			// objects are too far - ignore them
			if (abs(p0.x - p1.x) > dist || abs(p0.y - p1.y) > dist) return false;

			// objects are moveing in an assesmbly - they are linked
			if (abs(obj->dx - dx) < eps && abs(obj->dy - dy) < eps) return true;

			// cannot say anything definite on these two linkage - let's check it next time
			return false;
		}

		// update the object's coordinates it's invoked when we suppose that have found the object presence from the previous frame on the current one
                void updateCoords(cv::Point newCenter) {

			// we saw the object one more time
			totalFrames++;
            if(totalFrames > 10000) totalFrames = 1;

			// update the object path
			tracks.push_back(newCenter);

			// don't keep too much path segments
			if (tracks.size() > MAX_TRACK_LENGTH) {
				tracks.erase(tracks.begin());
			}

			// if the path is longer than 1 segment - try to assess the object's movement direction
			if (tracks.size() > 1) {
				// set last object offset
				dx = tracks[tracks.size() - 1].x - tracks[tracks.size() - 2].x;
				dy = tracks[tracks.size() - 1].y - tracks[tracks.size() - 2].y;

				// assess of the object moves inside or outside the zone of interest
				int sy = tracks[tracks.size() - 1].y - tracks[0].y;
				if (sy < 5) direction = Direction::InsideDirection;
				else if (sy > 5) direction = Direction::OutsideDirection;
				else direction = Direction::UnknownDirection;
			}

			// update the object's movement range on Y axis
			if (newCenter.y < minY) minY = newCenter.y;
			if (newCenter.y > maxY) maxY = newCenter.y;

			return;
		}

		// object was seen one more frame ago
		void updateFrames() { frames++; return; };

		// the object was seen right on the last frame
		void resetFrames() { frames = 0; return; };
	};

	// an object possible pretender relation
	// as all the detected artfacts from the current frame we call the `max` (maximum probable detection) we call it the `Object` <-> `Max` relation
	/*class ObjectMax {
	public:
		// the object index at the `objects` array we pretend to be
		int objectIdx;
		// the distance we are stand from the object we pretend to be
		double length;

		// the new object direction if we are really would be this object
		Object::Direction direction;
	};*/

	// a pretender record or a maximum probable detection
	class Pretender {
	public:
		// the detection center
                cv::Point center;

		// the pretender contour - just for visualization
                vector<cv::Point> contour;

		// the list of relations for this detection and the object it pretend to be
		//vector<ObjectMax> oms;
	};

	// The mapping table record it links a pretender and an object and keeps the distance between them
	class ClosePretender {
	public:
		// the predetected object index we pretend to be
		int objectIdx;
		// the detection index
		int maxIdx;
		// the distance between them
		double length;
	};

	// the object counter configuration class with predefined values for our first use case with a trolleybus and SoftKinetic camera
	class ObjectCounterConfig {
	public:
		// the low and high thresholds to filter too close, too far and noisy pixels
        int lowThreshold = 40;
        int highThreshold = 400;

		// a structre element size for morhological noise reduction
		int erosion_size = 15;

		// detected object minimum square
		int minSquare = 30;

		// the maximum distance an object can `jump` in one frame
		double sNear = 240 / 3.0;

		// the outer barrier - closer the door
        int outerBarrier = 40;

		// the inner barrier - farther the door
        int innerBarrier = 400;

		// how long we could remember an object in frame count
		int maxObjectFrames = 6;
	};

	// the object (Head) counter class
	class ObjectCounter
	{
		// an object took in callback
		std::function<void(shared_ptr<Object> obj)> onObjectIn;
		// an object took out callback
		std::function<void(shared_ptr<Object> obj)> onObjectOut;

		// all detected and tracking objects array
		vector<shared_ptr<Object>> objects;

		// the last object identifier
		int goid = 0;

		// linked objects - a sets of detected objects that could be parts of one person
		// the key indentifies the an object id, all the linked objects start to share the same `oid` as they was supposed linked
		map<long, vector<shared_ptr<Object>>> linked;
	public:
        long cnt_in = 0;
        long cnt_out = 0;

        // the config
		ObjectCounterConfig config;

		// Construct the counter object, set the callback that would be invoked on a possible person in/out pass
		ObjectCounter(std::function<void(shared_ptr<Object> obj)> objectIsInCallback, std::function<void(shared_ptr<Object> obj)> objectIsOutCallback) {
			onObjectIn = objectIsInCallback;
			onObjectOut = objectIsOutCallback;
		}

		// the main logic - count the person in or out useing a new frame in the `image` parameter
        void count(cv::Mat image, bool convertFromBGR = true, bool useThresholds = true, cv::Mat *color = nullptr, bool reverse = false) {
			//////////////////////////////////
			//                              //
			//       PREPARE THE FRAME      //
			//                              //
			//////////////////////////////////

			// grayscale the image first if it was not yet
            cv::Mat gray;

            if(convertFromBGR) cv::cvtColor(image, gray, CV_BGR2GRAY);
            else image.copyTo(gray);

			int i, j;
			if (useThresholds) {
				int grad = 255 / 4;//(config.highThreshold - config.lowThreshold) / 5;

				// filter the pixels we coul not use for correct detection
				// and inverse the value (for visual convinience during a debug)
				for (j = 0; j < gray.rows; j++) {
					for (i = 0; i < gray.cols; i++) {
						int ind = gray.data[j * gray.cols + i];

						if (ind > config.lowThreshold && ind < config.highThreshold) {
							ind = ind / grad*grad;
							ind = 255 - ind;
						}
						else ind = 0;

						gray.data[j * gray.cols + i] = ind;
					}
				}
			}

			// reduce the noise
			// first - simply filter the highest frequencies with a smooth filter
			//cv::blur(gray, gray, cv::Size(3, 3));

            if(config.erosion_size > 0) {
                // then try to restore lost pixels with morphological openning
                cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(config.erosion_size, config.erosion_size));
                //cv::morphologyEx(gray, gray, cv::MORPH_OPEN, kernel);
				cv::morphologyEx(gray, gray, cv::MORPH_ERODE, kernel);
                //cv::morphologyEx(gray, gray, cv::MORPH_DILATE, kernel);
                // at least - smooth the openning result to have more natural image with no sharp drops useing the same element size
                cv::blur(gray, gray, cv::Size(config.erosion_size, config.erosion_size));
            }

            //if (color != nullptr) {
            //    cv::imshow("erosion", gray);
            //}

			// the detected pretender array
			vector<shared_ptr<Pretender>> maxs;

			// the detected contours array
            vector<vector<cv::Point> > contours;
            vector<cv::Vec4i> hierarchy;

			//////////////////////////////////
			//                              //
			//       DETECT NEW OBJECTS     //
			//                              //
			//////////////////////////////////

			// keep a copy of an input image for contour detection
            cv::Mat c = gray.clone();
            cv::findContours(c, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

			// go through the detected contour array
			for (i = 0; i< contours.size(); i++)
			{
				// get the bounding rect
                cv::Rect br = boundingRect(contours[i]);

				// filter a countours with too small area
                if (br.width < config.minSquare || br.height < config.minSquare) continue;

				// get the bounding rect center and store a new detection in maxs array
                cv::Point objCenter = cv::Point((int)(br.x + br.width / 2), (int)(br.y + br.height / 2));
				shared_ptr<Pretender> max = make_shared<Pretender>();
				max->center = objCenter;
				max->contour = contours[i];
				maxs.push_back(max);

                if (color != nullptr) 
					cv::drawContours(*color, contours, i, cv::Scalar(0, 0, 255), 2, 8, hierarchy, 0, cv::Point());
					//cv::rectangle(*color, br, cv::Scalar(0, 0, 255), 2, 8);
			}

			/////////////////////////////////////////////////
			//						                       //
			//       ASSIGN NEW OBJECT TO THE OLD ONES     //
			//                                             //
			/////////////////////////////////////////////////

			// if have not any objects detected before - just keep the list of all we have now
			if (objects.size() == 0) {
				for (i = 0; i < maxs.size(); i++) {
					shared_ptr<Object> o = make_shared<Object>(goid++, maxs[i]->center);
					objects.push_back(o);

                    if(goid > 10000) goid = 1;
				}
			}
			else {
				// an array to keep the fact if the object with corresponding index is already found on the current frame
				vector<bool> objectFound;
				// an array to keep the fact if the detection with corresponding index already found in the previously detected object list
				vector<bool> maxFound;
				// the pretender <-> object relations to assess the closest ones
				vector<ClosePretender> closePretenders;

				// initialize the `found` arrays - nothing was found yet for the current frame
				for (i = 0; i < maxs.size(); i++) maxFound.push_back(false);
				for (i = 0; i < objects.size(); i++) objectFound.push_back(false);

				// go through the pretender list and build a distance map
				for (i = 0; i < maxs.size(); i++) {
					for (j = 0; j < objects.size(); j++) {
						// assess the distance between the i-detection and the j-object
                                                cv::Point pt = objects[j]->tracks[objects[j]->tracks.size() - 1];
						double length = sqrt(pow(maxs[i]->center.x - pt.x, 2) + pow(maxs[i]->center.y - pt.y, 2));

						// assess the new object direction if this detection would be it
						Object::Direction direction = Object::Direction::UnknownDirection;
						int dy = maxs[i]->center.y - pt.y;
						if (dy < 5) direction = Object::Direction::InsideDirection;
						else if (dy > 5) direction = Object::Direction::OutsideDirection;

						// drop it if the object offset would be too long for this detection
						if (length > config.sNear) continue;

						// create a new detection <-> object relation
						ClosePretender cp;
						cp.length = length;
						cp.objectIdx = j;
						cp.maxIdx = i;
						// if the new direction is different from the previous object direction - multiply it twice to give more chances for
						// the same direction pretenders
						if (direction != objects[j]->direction) cp.length *= 2;
						closePretenders.push_back(cp);
					}
				}

				// sort all the pretenders with the distance which is depend not only on the real distance but on the direction too
				std::sort(closePretenders.begin(), closePretenders.end(), [](const ClosePretender& lhs, const ClosePretender& rhs)
				{
					return lhs.length < rhs.length;
				});

				// search for new objects coordinates on the current frame
				for (int i = 0; i < closePretenders.size(); i++) {
					// if the object is already assigned to a new detection - skip the pretender
					// if the maximum is already assigned to an obejct - skip the pretender
					if (objectFound[closePretenders[i].objectIdx] || maxFound[closePretenders[i].maxIdx]) continue;

					// get vars to keep the code shorter
					int oi = closePretenders[i].objectIdx;
					int mi = closePretenders[i].maxIdx;

					// assign the closest detection to an object
					objects[oi]->updateCoords(maxs[mi]->center);
					objects[oi]->resetFrames();

					// say the object was found and the detection was assigned
					objectFound[oi] = true;
					maxFound[mi] = true;
				}

				// if we still have unassigned detections - create new objects for them
				for (i = 0; i < maxFound.size(); i++) {
					if (!maxFound[i]) {
						shared_ptr<Object> o = make_shared<Object>(goid++, maxs[i]->center);
						objects.push_back(o);
					}
				}
			}

			//
			// now we suppose that we have the new posistions of all the objects and it's trajectories
			//


			//////////////////////////////////
			//                              //
			//       ANALYSE THE MOTION     //
			//                              //
			//////////////////////////////////

			// analize the movements
			for (int i = 0; i < objects.size(); i++) {
                                vector<cv::Point> track = objects[i]->getTracks();

				// if the object lives longer than one frame - we can analize it's motion
				if (track.size() > 1) {
					// let's check if we habe a new information on syncronously moveing objects
					for (int j = 0; j < objects.size(); j++) {
						// we check the same object - skip
						if (j == i) continue;
						// we check already linked object - skip
						if (objects[i]->oid == objects[j]->oid) continue;

						// check if the objects move together
 						if (objects[i]->isLinked(objects[j], 20, config.sNear)) {
							// link objects togeher
 							long oldId = objects[j]->oid;
							// printf("link: %d + %d [%d;%d] [%d;%d]\n", objects[i]->oid, objects[j]->oid, objects[i]->dx, objects[i]->dy, objects[j]->dx, objects[j]->dy);
							addLinked(objects[i], objects[i]);
							addLinked(objects[j], objects[i]);

							// link other objects which was connected the the second object before we linked it with the current one
							if (linked.count(oldId) != 0) {
								for (int li = 0; li < linked[oldId].size(); li++) {
									addLinked(objects[j], linked[oldId][li]);
								}
								linked[oldId].clear();
							}
						}
					}

                                        cv::polylines(*color, track, false, cv::Scalar(255, 255, 0), 2);

   					// check for crosses
					checkCross(objects[i], config.innerBarrier, config.outerBarrier);

					// check if we have any new crosses
					//if ((innerCrossed(objects[i]) || outerCrossed(objects[i])) && !objects[i]->seen) {
					if (innerCrossed(objects[i]) && outerCrossed(objects[i]) && !objects[i]->seen) {
						// filter the new crosses for trajectory path to except the noisy objects and motions
						int yPath = 0;
						int totalFrames = 0;
						getSummaryYPath(objects[i], yPath, totalFrames);
						if (yPath > config.sNear/2 && totalFrames >= 5) {
							// if the trajectory looks good - fire a pass event
							if (getDirection(objects[i]) == Object::Direction::OutsideDirection) {
								//printf("Out: %d\n", objects[i]->oid);
								setSeen(objects[i]);

                                if(reverse) {
                                    cnt_in++;
                                    if(cnt_in > 1000000) cnt_in = 1;

                                    if (onObjectIn != nullptr) {
                                        onObjectIn(objects[i]);
                                    }
                                } else {
                                    cnt_out++;
                                    if(cnt_out > 1000000) cnt_out = 1;

                                    if (onObjectOut != nullptr) {
                                        onObjectOut(objects[i]);
                                    }
                                }
							}
							if (getDirection(objects[i]) == Object::Direction::InsideDirection) {
								//printf("In: %d\n", objects[i]->oid);
								setSeen(objects[i]);

                                if(reverse) {
                                    cnt_out++;
                                    if(cnt_out > 1000000) cnt_out = 1;

                                    if (onObjectOut != nullptr) {
                                        onObjectOut(objects[i]);
                                    }
                                } else {
                                    cnt_in++;
                                    if(cnt_in > 1000000) cnt_in = 1;

                                    if (onObjectIn != nullptr) {
                                        onObjectIn(objects[i]);
                                    }
                                }
							}
						}
					}
				}

				if (color != nullptr) {
                                        cv::Point place = track[track.size() - 1];
					string dir = "u";
					if (objects[i]->direction == Object::Direction::InsideDirection) dir = "i";
					else if (objects[i]->direction == Object::Direction::OutsideDirection) dir = "o";
                                        cv::putText(*color, "Pid: " + to_string(objects[i]->oid) + " " + (innerCrossed(objects[i]) ? "1" : "x ") + (outerCrossed(objects[i]) ? "1" : "x ") + dir, place, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 255), 2);
				}

				// update object life time
				objects[i]->updateFrames();

				// forget an old object
				if (objects[i]->frames > config.maxObjectFrames) {
					removeLinked(objects[i]);
					objects.erase(objects.begin() + i);
					i--;
				}
			}
			// remove `hanged` linked objects - the linked records for objects which already does not exist
			cleanLinked();

			if (color != nullptr) {
                                cv::line(*color,
                                        cv::Point(0, config.outerBarrier),            //Starting point of the line
                                        cv::Point(color->cols, config.outerBarrier),   //Ending point of the line
                                        cv::Scalar(255, 0, 0),               //Color
					2,                               //Thickness
					8);                              //Linetype
                                cv::line(*color,
                                        cv::Point(0, config.innerBarrier),            //Starting point of the line
                                        cv::Point(color->cols, config.innerBarrier),   //Ending point of the line
                                        cv::Scalar(0, 255, 0),               //Color
					2,                               //Thickness
					8);                              //Linetype

                                cv::putText(*color, "Count IN: " + to_string(cnt_in), cv::Point(0, color->rows - 30), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0), 2);
                                cv::putText(*color, "Count OUT: " + to_string(cnt_out), cv::Point(0, color->rows - 10), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0), 2);
			}
		}

private:
		// the the `seen` flag for the current object and all it's linked ones
		void setSeen(shared_ptr<Object> obj) {
			obj->seen = true;

			long oid = obj->oid;
			if (linked.count(obj->oid) != 0) {
				for (int i = 0; i < linked[oid].size(); i++) {
					linked[oid][i]->seen = true;
				}
			}
		}

		// check if the inner barrier crossed for the current and all it's linked objects
		bool innerCrossed(shared_ptr<Object> obj) {
			if (obj->_innerCrossed) return obj->_innerCrossed;

			if (linked.count(obj->oid) != 0) {
				long oid = obj->oid;
				for (int i = 0; i < linked[oid].size(); i++)
					if (linked[oid][i]->_innerCrossed) return true;
			}

			return false;
		}

		// the same fir the outer barrier
		bool outerCrossed(shared_ptr<Object> obj) {
			if (obj->_outerCrossed) return obj->_outerCrossed;

			if (linked.count(obj->oid) != 0) {
				long oid = obj->oid;
				for (int i = 0; i < linked[oid].size(); i++)
					if (linked[oid][i]->_outerCrossed) return true;
			}

			return false;
		}

		// get the object general direction based on it's own and linked objects movements
		Object::Direction getDirection(shared_ptr<Object> obj) {
			long oid = obj->oid;

			int inDir = obj->direction == Object::Direction::InsideDirection ? 1 : 0;
			int outDir = obj->direction == Object::Direction::OutsideDirection ? 1 : 0;

			if (linked.count(oid) != 0) {
				for (int i = 0; i < linked[oid].size(); i++) {
					if (linked[oid][i]->direction == Object::Direction::InsideDirection) inDir++;
					if (linked[oid][i]->direction == Object::Direction::InsideDirection) outDir++;
				}
			}

			if (inDir > outDir) return Object::Direction::InsideDirection;
			if (inDir < outDir) return Object::Direction::OutsideDirection;

			return Object::Direction::UnknownDirection;
		}

		// link one object to another
		void addLinked(std::shared_ptr<Object> obj, std::shared_ptr<Object> oObj) {
			if (linked.count(oObj->oid) > 0) {
				for (int i = 0; i < linked[oObj->oid].size(); i++) {
					if (linked[oObj->oid][i] == obj)  return;
				}
			}

			if (linked.count(oObj->oid) == 0) {
				linked.insert(std::pair<long, vector<shared_ptr<Object>>>(oObj->oid, vector<shared_ptr<Object>>()));
			}
			linked[oObj->oid].push_back(obj);

			obj->oid = oObj->oid;

			for (int i = 0; i < linked[oObj->oid].size(); i++) {
				linked[oObj->oid][i]->seen = obj->seen || oObj->seen;
				linked[oObj->oid][i]->_innerCrossed = obj->_innerCrossed || oObj->_innerCrossed;
				linked[oObj->oid][i]->_outerCrossed = obj->_outerCrossed || oObj->_outerCrossed;
			}
		}

		//unlink objects
		void removeLinked(std::shared_ptr<Object> obj) {
			long oid = obj->oid;

			if (linked.count(oid) == 0) return;

			for (int i = 0; i < linked[oid].size(); i++) {
				if (linked[oid][i] == obj) {
					linked[oid].erase(linked[oid].begin() + i);
					break;
				}
			}
		}

		// clean the `hanged` object linkage arrays
		void cleanLinked() {
			vector<long> removeKeys;
			for (map<long, vector<shared_ptr<Object>>>::iterator i = linked.begin(); i != linked.end(); ++i) {
				if (i->second.size() <= 1) removeKeys.push_back(i->first);
			}

			for (long i = 0; i < removeKeys.size(); i++) linked.erase(removeKeys[i]);
		}

		// check if we crossed the inner and outter barriers and set the crossed flags for the current and all the linked objects
		void checkCross(shared_ptr<Object> obj, int inner, int outer) {
			int minY = 1000000;
			int maxY = -1000000;

			if (obj->minY < minY) minY = obj->minY;
			if (obj->maxY > maxY) maxY = obj->maxY;

			long oid = obj->oid;
			if (linked.count(obj->oid) > 0) {
				for (int i = 0; i < linked[oid].size(); i++) {
					if (linked[oid][i]->minY < minY) minY = linked[oid][i]->minY;
					if (linked[oid][i]->maxY > maxY) maxY = linked[oid][i]->maxY;
				}
			}

			if (minY <= inner && maxY > inner) {
				obj->_innerCrossed = true;

				if (linked.count(obj->oid) > 0) {
					for (int i = 0; i < linked[oid].size(); i++) {
						linked[oid][i]->_innerCrossed = true;
					}
				}
			}

			if (minY <= outer && maxY > outer) {
				obj->_outerCrossed = true;

				if (linked.count(obj->oid) > 0) {
					for (int i = 0; i < linked[oid].size(); i++) {
						linked[oid][i]->_outerCrossed = true;
					}
				}
			}
		}

		// estimate the Y axis path length
		void getSummaryYPath(shared_ptr<Object> obj, int &path, int &pc) {
			if (linked.count(obj->oid) > 0) {
				path = 0;
				pc = obj->totalFrames;
				for (int i = 0; i < linked[obj->oid].size(); i++) {
					path += abs(linked[obj->oid][i]->maxY - linked[obj->oid][i]->minY);
					if (linked[obj->oid][i]->totalFrames > pc) pc = linked[obj->oid][i]->totalFrames;
				}

				return;
			}
			else {
				path =  abs(obj->maxY - obj->minY);
				pc = obj->totalFrames;
			}
		}
	};
}
