#include "serverconnection.h"

ServerConnection::ServerConnection(QObject *parent, ServerConnectionListener *listener) :
    QObject(parent),
    m_pWebSocketServer(new QWebSocketServer(QStringLiteral("Onboard Server"), QWebSocketServer::NonSecureMode, this))
{
    qRegisterMetaType<std::function<void(QWebSocket* connection, QJsonObject args)>>("std::function<void(QWebSocket* connection, QJsonObject args)>");
    qRegisterMetaType<QWebSocket*>("QWebSocket*");

    QObject::connect(&m_webSocket, &QWebSocket::connected, this, &ServerConnection::onConnected);
    QObject::connect(&m_webSocket, &QWebSocket::disconnected, this, &ServerConnection::onClosed);
    QObject::connect(&m_webSocket, &QWebSocket::textMessageReceived, this, &ServerConnection::onServerMessageReceived);

    if (m_pWebSocketServer->listen(QHostAddress::Any, 9002)) {
        qDebug() << "Echoserver listening on port" << 9002;
        printf("Connect...\n");
        QObject::connect(m_pWebSocketServer, &QWebSocketServer::newConnection, this, &ServerConnection::onServiceConnected);
        printf("Connect...done\n");
    }

    this->listener = listener;
    printf("ServerConnection cons...done\n");
}

ServerConnection::~ServerConnection() {
    ServerConnection::disconnect();
    if(m_serviceSocket != nullptr) m_serviceSocket->close();
}

ServerConnection::ConnectionState ServerConnection::getState() {
    if(state == ConnectionState::PENDING) {
        if(pendingStateStart - QDateTime::currentMSecsSinceEpoch() >= 3000) state = ConnectionState::DISCONNECTED;
    }

    return state;
}

bool ServerConnection::connect(QString url) {
    std::lock_guard<std::mutex> lock(callCS);
    if(state != ConnectionState::DISCONNECTED) return false;

    m_url = url;
    try {
        m_webSocket.open(QUrl(url));
    } catch(...) {
        return false;
    }

    state = ConnectionState::PENDING;

    return true;
}

bool ServerConnection::disconnect() {
    std::lock_guard<std::mutex> lock(callCS);
    if(state == ConnectionState::DISCONNECTED) return false;

    pendingDisconnect = true;

    try {
        m_webSocket.close();
    } catch(...) {}

    state = ConnectionState::DISCONNECTED;

    return true;
}

bool ServerConnection::call(QWebSocket *connection, QString interface, QString method, QJsonObject args, std::function<void(QWebSocket *connection, QJsonObject args)> onResult, std::function<void(QWebSocket *connection, QJsonObject args)> onFail) {
    long long id = callId;

    {
        std::lock_guard<std::mutex> lock(callCS);
        if(connection == &m_webSocket)
            if(state == ConnectionState::DISCONNECTED) return false;

        callId++;
        if(callId > 1000000) callId = 1;
        callbacks[id] = Callback();
        callbacks[id].set(onResult, onFail);
    }

    try {
        QJsonObject request;
        request["id"] = id;
        request["type"] = "query";
        request["interface"] = interface;
        request["method"] = method;
        request["args"] = args;

        QJsonDocument doc(request);
        connection->sendTextMessage(QString(doc.toJson()));
    } catch(...) {
        return false;
    }

    return true;
}

bool ServerConnection::sendBinary(QWebSocket *connection, QByteArray data) {
    try {
        connection->sendBinaryMessage(data);
    } catch(...) {

    }

    return false;
}

bool ServerConnection::sendText(QWebSocket *connection, QString data) {
    try {
        connection->sendTextMessage(data);
    } catch(...) {

    }

    return false;
}

bool ServerConnection::answer(QWebSocket *connection, int id, QJsonObject args) {
    {
        std::lock_guard<std::mutex> lock(callCS);
        if(connection == &m_webSocket)
            if(state == ConnectionState::DISCONNECTED) return false;
    }

    try {
        QJsonObject answer;
        answer["id"] = id;
        answer["type"] = "answer";
        answer["args"] = args;

        QJsonDocument doc(answer);
        connection->sendTextMessage(QString(doc.toJson()));
    } catch(...) {
        return false;
    }

    return true;
}

bool ServerConnection::error(QWebSocket *connection, int id, QString message) {
    {
        std::lock_guard<std::mutex> lock(callCS);
        if(connection == &m_webSocket)
            if(state == ConnectionState::DISCONNECTED) return false;
    }

    try {
        QJsonObject message;
        message["error"] = message;

        QJsonObject error;
        error["id"] = id;
        error["type"] = "error";
        error["args"] = message;

        QJsonDocument doc(error);
        connection->sendTextMessage(QString(doc.toJson()));
    } catch(...) {
        return false;
    }

    return true;
}

void ServerConnection::onConnected() {
    std::lock_guard<std::mutex> lock(callCS);
    state = ConnectionState::CONNECTED;

    emit connected();

    if(listener != nullptr) listener->onConnected();
}

void ServerConnection::onServerMessageReceived(QString message) {
    onTextMessageReceived(&m_webSocket, message);
}

void ServerConnection::onServiceMessageReceived(QString message) {
    onTextMessageReceived(m_serviceSocket, message);
}

void ServerConnection::onTextMessageReceived(QWebSocket *connection, QString message)
{
    QJsonDocument doc(QJsonDocument::fromJson(message.toUtf8()));
    QJsonObject _message = doc.object();

    try {
        QString type = _message["type"].toString();
        int id = _message["id"].toInt();
        if(type == "error" || type == "answer") {
            if(callbacks.contains(id)) {
                try {
                    Callback callback = callbacks[id];
                    if(type == "error") {
                        if(callback.onFail != nullptr) callback.onFail(connection, _message["args"].toObject());
                    } else {
                        if(callback.onResult != nullptr) callback.onResult(connection, _message["args"].toObject());
                    }
                } catch(...) {}
                callbacks.remove(id);
            } else {}
        } else if(type == "query") {
            QString interfaceName = _message["interface"].toString();
            QString methodName = _message["method"].toString();
            if(iface.contains(interfaceName)) {
                QMap<QString, std::function<void(QWebSocket *connection, int id, QJsonObject args)>> methods = iface[interfaceName];
                if(methods.contains(methodName)) {
                    try {
                        methods[methodName](connection, id, _message["args"].toObject());
                    } catch(...) {
                        error(connection, _message["id"].toInt(), "Unexpected error");
                    }
                } else {
                    error(connection, _message["id"].toInt(), "No method " + interfaceName + "." + methodName);
                }
            } else {
                error(connection, _message["id"].toInt(), "No interface " + interfaceName);
            }
        }
    } catch(...){};
}

void ServerConnection::onClosed() {
    if(pendingDisconnect) {
        pendingDisconnect = false;
        return;
    }

    std::lock_guard<std::mutex> lock(callCS);
    state = ConnectionState::DISCONNECTED;

    emit closed();

    if(listener != nullptr) listener->onDisconnected();
}

void ServerConnection::onServiceConnected() {
    qDebug() << "service connected";

    m_serviceSocket = m_pWebSocketServer->nextPendingConnection();
    QObject::connect(m_serviceSocket, &QWebSocket::textMessageReceived, this, &ServerConnection::onServiceMessageReceived);
    QObject::connect(m_serviceSocket, &QWebSocket::disconnected, this, &ServerConnection::onServiceClosed);

    if(listener != nullptr) listener->onServiceConnected();
}

void ServerConnection::onServiceClosed() {
    qDebug() << "service disconnected";

    if(listener != nullptr) listener->onServiceDisconnected();

    m_serviceSocket = nullptr;
}
