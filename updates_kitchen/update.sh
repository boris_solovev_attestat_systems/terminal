#!/bin/sh

dir="$PWD"

echo "kill previous"
kill -TERM $1

sleep 10

echo "Clean target"
rm /home/attistat/attistat/adistr/*

echo "Copy files..."
cp -r /home/attistat/attistat/updates/temp/* /home/attistat/attistat/adistr/

echo "Start software"
cd /home/attistat/attistat/adistr/
./terminal > out